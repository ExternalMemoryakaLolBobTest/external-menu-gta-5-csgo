#include <GDIPlus.au3>
#include <WindowsConstants.au3>
Local $hGUI, $hGraphics, $hBitmap, $hBackBuffer, $hPen, $wedith = @DesktopWidth/2, $height = @DesktopHeight/2
$EntityPos = DllStructCreate("struct; float Kopf; float BeinL; float BeinR; float ArmL; float ArmR; float Bauch; endstruct")
_GDIPlus_Startup()


$hGUI = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, $WS_POPUP, BitOR($WS_EX_LAYERED, $WS_EX_TOPMOST,$WS_EX_TRANSPARENT))
GUISetBkColor(0xABCDEF, $hGUI)
_WinAPI_SetLayeredWindowAttributes($hGUI, 0xABCDEF, 255)

$hPen = _GDIPlus_PenCreate(0xFFFF0000,2)
$hGraphics = _GDIPlus_GraphicsCreateFromHWND($hGUI)
$hBitmap = _GDIPlus_BitmapCreateFromGraphics(@DesktopWidth, @DesktopHeight, $hGraphics)
$hBackBuffer = _GDIPlus_ImageGetGraphicsContext($hBitmap)
_GDIPlus_GraphicsSetSmoothingMode($hBackBuffer, $GDIP_SMOOTHINGMODE_HIGHQUALITY) ;sets the graphics object rendering quality (antialiasing)
GUISetState()

Func GDIP()
GDIPClean()
_GDIPlus_GraphicsDrawString($hBackBuffer, "ESP Key 0 " & $34, 10, 10)
_GDIPlus_GraphicsDrawString($hBackBuffer, "Aimbot Only Head Key 8 " & $37, 10, 30)
_GDIPlus_GraphicsDrawString($hBackBuffer, "Wall Aim Key 7 " & $38, 10, 50)
_GDIPlus_GraphicsDrawString($hBackBuffer, "Auto Aim Key 9 " & $39, 10, 70)
_GDIPlus_GraphicsDrawString($hBackBuffer, "Trigger AimBot Key 4 " & $30, 10, 90)

GDIPRender()
EndFunc

Func GDIPClean()
 _GDIPlus_GraphicsClear($hBackBuffer, 0xFF000000 + 0xABCDEF)
EndFunc

Func GDIPRender()
_GDIPlus_GraphicsDrawImage($hGraphics, $hBitmap, 0, 0)
EndFunc
Func _Exit()
    _GDIPlus_GraphicsDispose($hGraphics)
    _GDIPlus_BitmapDispose($hBitmap)
    _GDIPlus_PenDispose($hPen)
	_GDIPlus_GraphicsDispose($hGraphic)
    GUIDelete($hGUI)
    Exit
EndFunc