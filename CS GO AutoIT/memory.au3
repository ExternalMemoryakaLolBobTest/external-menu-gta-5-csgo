
#include-once

;PUBLIC CONSTANTS
Global Const $BYTE = "BYTE"
Global Const $BOOL = "BOOLEAN"
Global Const $INT = "INT"
Global Const $FLOAT = "FLOAT"

;INTERNAL
Global $__KERNEL32 = DllOpen("kernel32.dll")
Global $__ProcessID = 0

;INTERNAL CONST
Global Const $__READ = 0x10
Global Const $__WRITE = 0x28

;PUBLIC FUNCTION
Func _MemorySelectProcess($name)
	$__ProcessID = ProcessExists($name)
	Return $__ProcessID <> 0
EndFunc

;Determine the base address of a module within process. return 0 == not found
Func _MemoryGetModuleBase($name)
	$struct = _
        'DWORD dwSize;' & _
        'DWORD th32ModuleID;' & _
        'DWORD th32ProcessID;' & _
        'DWORD GlblcntUsage;' & _
        'DWORD ProccntUsage;' & _
        'ptr modBaseAddr;' & _
        'DWORD modBaseSize;' & _
        'ptr hModule;' & _
        'CHAR szModule[256];' & _
        'CHAR szExePath[260]'
	$ModuleEntry = DllStructCreate($struct)
	$SnapShot = DllCall($__KERNEL32, 'ptr', 'CreateToolhelp32Snapshot', 'DWORD', BitOR(0x8, 010), 'DWORD', $__ProcessID)[0]
	if   @error Then Return 0
	$ModuleEntry.dwSize = DllStructGetSize($ModuleEntry)
	$bFirst = DllCall($__KERNEL32, 'BOOL', 'Module32First', 'ptr', $SnapShot, 'ptr', DllStructGetPtr($ModuleEntry))[0]
	if Not $bFirst Or   @error Then Return 0
	While True
		If StringInStr($ModuleEntry.szExePath, $name) Then
			DllCall($__KERNEL32, 'BOOL', 'CloseHandle', 'ptr', $SnapShot)
			Return $ModuleEntry.modBaseAddr
		EndIf
		$bFirst = DllCall($__KERNEL32, 'BOOL', 'Module32Next', 'ptr', $SnapShot, 'ptr', DllStructGetPtr($ModuleEntry))[0]
		if $bFirst == 0x12 Or   @error Then
			DllCall($__KERNEL32, 'BOOL', 'CloseHandle', 'ptr', $SnapShot)
			Return 0
		EndIf
	WEnd
EndFunc

;GENERIC READ/Write
Func _MemoryRead($type, $address)
	$hBuffer = DllStructCreate($type)
	$hProc = __OpenProcess($__ProcessID, $__READ)
	DllCall($__KERNEL32, "int", "ReadProcessMemory", "int", $hProc, "int", $address, "ptr", DllStructGetPtr($hBuffer), "int", DllStructGetSize($hBuffer), "int", 0)
	__CloseProcess($hProc)
	if   @error Then Return 0
	Return DllStructGetData($hBuffer,1)
EndFunc

;READ AND WRITE C STYLED STRUCT
Func _MemoryReadStruct($struct, $address)
	$hProc = __OpenProcess($__ProcessID, $__READ)
	DllCall($__KERNEL32, "int", "ReadProcessMemory", "int", $hProc, "int", $address, "ptr", DllStructGetPtr($struct), "int", DllStructGetSize($struct), "int", 0)
	__CloseProcess($hProc)
	if   @error Then Return 0
	Return $struct
EndFunc

;PRIVATE FUNCTION
Func __OpenProcess($pid, $access)
	$hProcess = DllCall($__KERNEL32, "int", "OpenProcess", "int", $access, "int", 1, "int", $pid)
	if   @error Then Return 0
	return $hProcess[0]
EndFunc

Func __CloseProcess($hProc)
	DllCall($__KERNEL32, "int", "CloseHandle", "int", $hProc)
EndFunc