Func _Convert_To_Binary($iNumber)
    Local $sBinString = ""
    Do
        $sBinString = BitAND($iNumber, 1) & $sBinString
        $iNumber = BitShift($iNumber, 1)
    Until $iNumber <= 0
    If $iNumber < 0 Then SetError(1, 0, 0)
    Return $sBinString
 EndFunc   ;==>_Convert_To_Binary

Func _IsVisibility($iNumber,$iLocalIndex)
Local $iLength = StringLen(_Convert_To_Binary($iNumber))
Local $sString = StringMid(_Convert_To_Binary($iNumber),$iLength-$iLocalIndex , 1)
Return($sString)
EndFunc

Func _IsVisibilityOld($iNumber,$iLocalIndex)
Local $iLength = StringLen($iNumber)
Local $sString = StringMid($iLength-$iLocalIndex , 1)
Return($sString)
EndFunc