#include <GDIPlus.au3>
#include <WindowsConstants.au3>
Local $hGUI, $hGraphics, $hBitmap, $hBackBuffer, $hPen, $wedith = @DesktopWidth/2, $height = @DesktopHeight/2
$EntityPos = DllStructCreate("struct; float Kopf; float BeinL; float BeinR; float ArmL; float ArmR; float Bauch; endstruct")
_GDIPlus_Startup()


$hGUI = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, $WS_POPUP, BitOR($WS_EX_LAYERED, $WS_EX_TOPMOST,$WS_EX_TRANSPARENT))
GUISetBkColor(0xABCDEF, $hGUI)
_WinAPI_SetLayeredWindowAttributes($hGUI, 0xABCDEF, 255)

$hPen = _GDIPlus_PenCreate(0xFFFF0000,2)
$hGraphics = _GDIPlus_GraphicsCreateFromHWND($hGUI)
$hBitmap = _GDIPlus_BitmapCreateFromGraphics(@DesktopWidth, @DesktopHeight, $hGraphics)
$hBackBuffer = _GDIPlus_ImageGetGraphicsContext($hBitmap)
;~ _GDIPlus_GraphicsSetSmoothingMode($hBackBuffer, $GDIP_SMOOTHINGMODE_HIGHQUALITY) ;sets the graphics object rendering quality (antialiasing)
GUISetState()

Func GDIPClean()
 _GDIPlus_GraphicsClear($hBackBuffer, 0xFF000000 + 0xABCDEF)
;~  _GDIPlus_GraphicsDispose($hBackBuffer)
EndFunc

Func GDIPRender()
_GDIPlus_GraphicsDrawImage($hGraphics, $hBitmap, 0, 0)
EndFunc

Func GDIP()
GDIPClean()

GDIPColor()

$GRadius = @desktopHeight / 22

_GDIPlus_GraphicsDrawEllipse($hBackBuffer, $w2sHitbox.X-1, $w2sHitbox.Y-1, 2, 2, $hPen)
_GDIPlus_GraphicsDrawEllipse($hBackBuffer, $aimPointX-$GRadius/2, $aimPointY-$GRadius/2, $GRadius, $GRadius, $hPen)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  $heal, $aimPointX-25, $aimPointY-50)
_GDIPlus_GraphicsDrawString($hBackBuffer,  GetHealth($closestEntity), $aimPointX-$GRadius/2, $aimPointY-$GRadius)

GDIPRender()
EndFunc
$GRadius = @desktopHeight / 22
$hFormat = _GDIPlus_StringFormatCreate()
$hFamily = _GDIPlus_FontFamilyCreate("Arial")
$hFont = _GDIPlus_FontCreate($hFamily, 12, 2)
$Ypitch = 1.0
Func GDIPString($value, $X, $Y)
	Local $sString =$value, $aInfo
    $hBrush = _GDIPlus_BrushCreateSolid(0xFFFFFFFF)
    $tLayout = _GDIPlus_RectFCreate($X, $Y - $GRadius* $Ypitch, 0, 0)
    $aInfo = _GDIPlus_GraphicsMeasureString($hBackBuffer, $sString, $hFont, $tLayout, $hFormat)
    _GDIPlus_GraphicsDrawStringEx($hBackBuffer, $sString, $hFont, $aInfo[0], $hFormat, $hBrush)
	$Ypitch = $Ypitch + 0.5
EndFunc

Func GDIPADD()
$GRadius = @desktopHeight / 22
GDIPColor()
_GDIPlus_GraphicsDrawEllipse($hBackBuffer, $w2sHitbox.X-1, $w2sHitbox.Y-1, 2, 2, $hPen)
_GDIPlus_GraphicsDrawEllipse($hBackBuffer, $aimPointX-$GRadius/2, $aimPointY-$GRadius/2, $GRadius, $GRadius, $hPen)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  $heal, $aimPointX-25, $aimPointY-50)
;~     _GDIPlus_GraphicsSetTextRenderingHint($hGraphics, $GDIP_TEXTRENDERINGHINT_ANTIALIASGRIDFIT)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  GetHealth($currentEntity), $aimPointX-$GRadius/2, $aimPointY-$GRadius, "Times new roman", 15, 0)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  GetLastPlaceName($currentEntity), $aimPointX-$GRadius/2, $aimPointY-$GRadius*1.5, "Times new roman", 15, 0)

   $Ypitch = 1.0
   GDIPString("Heal: " & GetHealth($currentEntity) & " " & GetArmor($currentEntity), $aimPointX-$GRadius/2, $aimPointY)
   GDIPString("LastPlace: " & GetLastPlaceName($currentEntity), $aimPointX-$GRadius/2, $aimPointY)

   if GetHasDefuser($currentEntity) = 1 Then
   GDIPString("HasDefuser", $aimPointX-$GRadius/2, $aimPointY)
   EndIf
   if GetIsDefusing($currentEntity) = 1 Then
   GDIPString("IsDefusing", $aimPointX-$GRadius/2, $aimPointY)
   EndIf

;~ GDIPRender()
EndFunc

$spottettemp = 0
$spottet = 1

Func GDIPColor()


   $spottet = See($currentEntity)
   if not $spottettemp = $spottet Then
   If not $spottet > 0  Then
   _GDIPlus_PenSetColor($hPen, 0xFFFF0000)
   Else
   _GDIPlus_PenSetColor($hPen, 0xFFFFFF00)

   If _IsVisibility(See($currentEntity),Index()-1) > 0 Then
	_GDIPlus_PenSetColor($hPen, 0xFF00FF00)
   EndIf

   EndIf
   EndIf
   $spottettemp = $spottet
EndFunc

Func _Exit()
    _GDIPlus_GraphicsDispose($hGraphics)
    _GDIPlus_BitmapDispose($hBitmap)
    _GDIPlus_PenDispose($hPen)
	_GDIPlus_GraphicsDispose($hGraphic)
    GUIDelete($hGUI)
    Exit
EndFunc