#include <Process.au3>

#RequireAdmin
;~ #include <GDIPluscallGUIMenu.au3>
;~ #include "Func.au3"

#include <Misc.au3>
#include <MsgBoxConstants.au3>

;~ $34 = "OFF"
;~ $37 = "OFF"
;~ $38 = "OFF"
;~ $39 = "OFF"
;~ $30 = "OFF"

$Timer = 100
$Timercounter = 0

Local $hDLL = DllOpen("user32.dll")

HotKeySet("{END}", "Terminate")
ProcessWait("csgo.exe")
Sleep(10000)

Run ("C:\Program Files (x86)\AutoIt3\AutoIt3.exe sourcnew.au3")
Run ("C:\Program Files (x86)\AutoIt3\AutoIt3.exe aadll.au3")
Run ("C:\Program Files (x86)\AutoIt3\AutoIt3.exe sourcsee.au3")

;~ GDIP()
While True

;~ If _IsPressed(34, $hDLL) Then
;~ If $34 = "OFF"  Then
;~ $34 = "ON"
;~ Else
;~ $34 = "OFF"
;~ EndIf
;~ GDIP()
;~ EndIf

;~ If _IsPressed(38, $hDLL) Then
;~ If $38 = "OFF"  Then
;~ $38 = "ON"
;~ Else
;~ $38 = "OFF"
;~ EndIf
;~ GDIP()
;~ EndIf

;~ If _IsPressed(37, $hDLL) Then
;~ If $37 = "OFF"  Then
;~ $37 = "ON"
;~ Else
;~ $37 = "OFF"
;~ EndIf
;~ GDIP()
;~ EndIf

;~ If _IsPressed(39, $hDLL) Then
;~ If $39 = "OFF"  Then
;~ $39 = "ON"
;~ Else
;~ $39 = "OFF"
;~ EndIf
;~ GDIP()
;~ EndIf

;~ If _IsPressed(30, $hDLL) Then
;~ If $30 = "OFF"  Then
;~ $30 = "ON"
;~ Else
;~ $30 = "OFF"
;~ EndIf
;~ GDIP()
;~ EndIf

$Timercounter = $Timercounter + 1

;~ If $Timercounter > $Timer  Then
;~    GDIPClean()
;~    GDIPRender()
;~    $Timercounter = 0
;~ EndIf

Sleep(100)
WEnd

Func Terminate()
   Run ("C:\Windows\System32\cmd.exe /c TASKKILL /F /IM AutoIt3.exe")
   Run ("C:\Windows\System32\cmd.exe /c TASKKILL /F /IM cmd.exe")
EndFunc   ;==>Terminate
