
#include "memory.h"
#include "stdafx.h"
//External Wrapper
DWORD SIZE_;
//https://gamerzhacking.com/index.php?threads/c-external-aob-pattern-scan.2043/
int ScanPatternEx(HANDLE hProc, int base, int len, char* sig, char* mask)
{
	auto buffer = new char[len + 1];
	memset(buffer, 0, len + 1);
	ReadProcessMemory(hProc, (void*)base, buffer, len, 0);

	if (ReadProcessMemory(hProc, (void*)base, buffer, len, NULL) == false)
	{
		char buf2[64];
		MessageBoxA(0, buf2, "Error", MB_OK);
		return NULL;
	}

	for (int i = 0; i < len; i++)
	{

		if ((buffer[i] == sig[0] && mask[0] == 'x') || (mask[0] == '?'))
		{
			for (int x = 0;; x++)
			{
				if (mask[x] == 'x')
				{
					if (buffer[i + x] == sig[x])
						continue;
					else
						break;
				}
				else if (mask[x] == 0x00)
				{
					delete[] buffer;
					return (int)(base + i);
				}
			}
		}
	}
	//delete buffer;

		/*for (int i = 0; i < len; i++)
	{
		if ((ReadMemory<BYTE>(hProc, base + i) == (BYTE)sig[0] && mask[0] == 'x') || (mask[0] == '?'))
		{
			for (int x = 0;; x++)
			{
				if (mask[x] == 'x')
				{
					if (ReadMemory<BYTE>(hProc, base + i + x) == (BYTE)sig[x])
						continue;
					else
						break;
				}
				else if (mask[x] == 0x00)
				{
					return (DWORD_PTR)(base + i);
				}
			}
		}
	}*/
}

int PointerScanPatternEx(HANDLE hProc, int base, int len, char* sig, char* mask,int offset, int extra)
{
	int patterna = ScanPatternEx(hProc, base, len, sig, mask);
	patterna = (ReadMemory<int>(hProc, (patterna + offset)) + extra) - base;
	return patterna;
}
/*

#include "memory.h"
#include "stdafx.h"
//External Wrapper
DWORD SIZE_;

DWORD_PTR ScanPatternEx(HANDLE hProc, DWORD_PTR base, DWORD len, char* sig, char* mask)
{
	BYTE* buf = (BYTE*)VirtualAlloc(0, len, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (ReadProcessMemory(hProc, (LPCVOID)base, buf, len, NULL) == false)
	{
		char buf2[64];
		MessageBoxA(0, buf2, "Error", MB_OK);
		return NULL;
	}
	for (int i = 0; i < len; i++)
	{
		if ((buf[i] == (BYTE)sig[0] && mask[0] == 'x') || (mask[0] == '?'))
		{
			for (int x = 0;; x++)
			{
				if (mask[x] == 'x')
				{
					if (buf[i + x] == (BYTE)sig[x])
						continue;
					else
						break;
				}
				else if (mask[x] == 0x00)
				{
					delete[] buf;
					return (DWORD_PTR)(base + i);
				}
			}
		}
	}
//	buf.Reset()
	//free(buf);
	//delete[] buf;
}

DWORD_PTR PointerScanPatternEx(HANDLE hProc, DWORD_PTR base, DWORD len, char* sig, char* mask)
{
DWORD64 patterna = ScanPatternEx(hProc, base, len, sig , mask);
patterna = patterna + (ReadMemory<int>(hProc, (patterna + 3)) + 7) - base;
return patterna;
}


*/