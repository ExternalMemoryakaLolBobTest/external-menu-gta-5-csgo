// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#pragma once
#ifndef _STDAFX_H_
#define _STDAFX_H_
#include "targetver.h"
#include <stdio.h>
#include <tchar.h>

#include "Hack.h"
#include "Memory.h"
#include "Menu.h"
#include "config.h"
#include "json.hpp"
//#include "D3D.h"
//#include "GDIPlus.h"

#include <windows.h>
#include <windowsx.h>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>
#include <psapi.h>
#include <vector>
#include <regex>

#include <Dwmapi.h>
#pragma comment(lib,"Dwmapi.lib")

#include "d3d9/d3d9.h"
#include "d3d9/d3dx9.h"
#pragma comment(lib,"d3d9/d3dx9.lib")
#pragma comment(lib,"d3d9/d3d9.lib")


#endif

// TODO: reference additional headers your program requires here

