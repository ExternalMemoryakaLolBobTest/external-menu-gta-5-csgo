#include <GDIPlus.au3>
#include <WindowsConstants.au3>
;~ #include <sourcsee.au3>
Local $hGUI, $hGraphics, $hBitmap, $hBackBuffer, $hPen, $wedith = @DesktopWidth/2, $height = @DesktopHeight/2
$EntityPos = DllStructCreate("struct; float Kopf; float BeinL; float BeinR; float ArmL; float ArmR; float Bauch; endstruct")
_GDIPlus_Startup()
;~ HotKeySet("{ESC}","_Exit")


$hGUI = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, $WS_POPUP, BitOR($WS_EX_LAYERED, $WS_EX_TOPMOST,$WS_EX_TRANSPARENT))
GUISetBkColor(0xABCDEF, $hGUI)
_WinAPI_SetLayeredWindowAttributes($hGUI, 0xABCDEF, 255)

$hPen = _GDIPlus_PenCreate(0xFFFF0000,2)
$hGraphics = _GDIPlus_GraphicsCreateFromHWND($hGUI)
$hBitmap = _GDIPlus_BitmapCreateFromGraphics(@DesktopWidth, @DesktopHeight, $hGraphics)
$hBackBuffer = _GDIPlus_ImageGetGraphicsContext($hBitmap)
_GDIPlus_GraphicsSetSmoothingMode($hBackBuffer, $GDIP_SMOOTHINGMODE_HIGHQUALITY) ;sets the graphics object rendering quality (antialiasing)
GUISetState()


    ;_GDIPlus_GraphicsDrawLine($hGraphics, $wedith-40, $height, $wedith+40, $height, $hPen)
    ;_GDIPlus_GraphicsDrawLine($hGraphics, $wedith+1, $height-40, $wedith+1, $height+40, $hPen)
    ;_GDIPlus_GraphicsDrawEllipse($hGraphics, $wedith-24, $height-24, 49, 49, $hPen)
    ;_GDIPlus_GraphicsDrawImage($hGraphics, $hBitmap, $wedith, $height)

Func GDIP()

;~ GDIPClean()
_GDIPlus_GraphicsDrawEllipse($hBackBuffer, $aimPointX-1, $aimPointY-1, 2, 2, $hPen)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  $heal, $aimPointX-25, $aimPointY-50)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer, "Lag Switch " & $Var_ONOFF, 10, 10)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer, "Sekunden " & $Bypass / 1000, 10, 30)
GDIPRender()
EndFunc

Func GDIPClean()
 _GDIPlus_GraphicsClear($hBackBuffer, 0xFF000000 + 0xABCDEF)
EndFunc

Func GDIPRender()
_GDIPlus_GraphicsDrawImage($hGraphics, $hBitmap, 0, 0)
EndFunc
Func _Exit()
    _GDIPlus_GraphicsDispose($hGraphics)
    _GDIPlus_BitmapDispose($hBitmap)
    _GDIPlus_PenDispose($hPen)
	_GDIPlus_GraphicsDispose($hGraphic)
    GUIDelete($hGUI)
    Exit
EndFunc