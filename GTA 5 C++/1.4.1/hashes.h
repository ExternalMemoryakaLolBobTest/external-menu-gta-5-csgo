#pragma once

const char* const pickupsname[] =
{
	"prop_poly_bag_01", "PICKUP_MONEY_CASE", "PICKUP_MONEY_SECURITY_CASE", "PICKUP_VEHICLE_MONEY_VARIABLE", "PICKUP_WEAPON_MINIGUN",
	"PICKUP_WEAPON_RPG", "PICKUP_WEAPON_MARKSMANRIFLE_MK2", "PICKUP_WEAPON_FIREWORK ", "PICKUP_SUBMARINE", "PICKUP_GANG_ATTACK_MONEY",
	"PICKUP_VEHICLE_HEALTH_STANDARD", "PICKUP_VEHICLE_ARMOUR_STANDARD", "PICKUP_VEHICLE_HEALTH_STANDARD", "PICKUP_MONEY_WALLET", "PICKUP_MONEY_PURSE", "PICKUP_MONEY_DEP_BAG",
	"PICKUP_MONEY_PAPER_BAG ", "PICKUP_HEALTH_STANDARD", "PICKUP_ARMOUR_STANDARD", "PICKUP_PORTABLE_CRATE_UNFIXED", "PICKUP_WEAPON_PROXMINE", "PistolMk2",
	"PICKUP_WEAPON_ADVANCEDRIFLE",
};
//https://wiki.gt-mp.net/index.php?title=Pickups
//int pickups[] =
//{-1296747938, 996550793, -214137936, -2121850769, -1835415205, 1948018762, -1127890446, -2115084258, 158843122, -95310859, -2124585240, -1945122029, 1850631618, -546236071, -1109887812, -1298986476, -1457529717, 2023061218, -1989692173, -253098439, 266812085, -2027042680, -1074893765, 990867623, -102572257, 582047296, -1118969278, -1112080475, -1997886297, 1577485217, 779501861, 1393009900, 693539241, 1311775952, -1661912808, -1093374267, 1765114797, -16088425, -1071729032, 663586612, -40063266, -668632385, -171582756, -1965167499, 127042729, -1621765815, -2050315855, 496339155, 792114228, -747492773, 768803961, 1983869217, 1587637620, -962731009, -1352061783, -105925489, 1817941018, 1234831722, 155106086, 1649373715, -1456120371, 1572258186, -462548556, 1632369836, 1835046764, 1295434569, -1766583645, 978070226, -282365040, 483787975, -30788308, -977852653, 1038697149, 157823901, 94531552, 2081529176, -48884066, -572254182, -336028321, -451800215}
const DWORD pickups[] =
{
	0xDE78F17E, 0xCE6FDD6B, 0xDE78F17E, 0x65948212, 0x2F36B434,	//"prop_poly_bag_01", "prop_money_bag_01", "PICKUP_MONEY_SECURITY_CASE", "PICKUP_VEHICLE_MONEY_VARIABLE", "PICKUP_AMMO_MINIGUN "
	0x4D36C349, 582047296, 0x22B15640, 0xE7CF07CC, 0xE175C698,	//"PICKUP_AMMO_RPG", "PICKUP_AMMO_RIFLE", "PICKUP_AMMO_FIREWORK_MP", "PICKUP_SUBMARINE", "PICKUP_GANG_ATTACK_MONEY",
	0x98D79EF, 0x4316CC09, 0x8F707C18, 0x5DE0AD3E, 0x1E9A99F8, 0x20893292,	//"PICKUP_VEHICLE_ARMOUR_STANDARD", "PICKUP_VEHICLE_HEALTH_STANDARD", "PICKUP_MONEY_WALLET", "PICKUP_MONEY_PURSE", "PICKUP_MONEY_DEP_BAG",
	0x14568F28, 0x711D02A4, 0x4BFB42D, 0x6E717A95, 0x624F7213, 0xBFE256D4, 	//"PICKUP_MONEY_PAPER_BAG ", "PICKUP_HEALTH_STANDARD", "PICKUP_ARMOUR_STANDARD", "PICKUP_PORTABLE_CRATE_UNFIXED", "PICKUP_WEAPON_PROXMINE ", "PistolMk2",
	0xB2B5325E,												//"PICKUP_WEAPON_ADVANCEDRIFLE", "TURISMO2", "ZTYPE"
};

const char* const carsname[] =
{
	"ADDER", "BANSHEE2", "BULLET", "CHEETAH", "ENTITYXF",
	"FMJ", "GP1", "SHEAVA", "INFERNUS", "OSIRIS",
	"LE7B", "PFISTER811", "PROTOTIPO", "REAPER", "SULTANRS",
	"T20", "TURISMOR", "TYRUS", "VACCA", "VOLTIC",
	"oppressor2" ,"akula", "vigilante", "Spacedocker", "stromberg",
	"Pfister811", "Scramjet", "Prototipo", "pbus2", "deluxo",
	"rapidgt3",
	"ZENTORNO",
	"Ambulance", "FIB Buffalo", "FIB Granger", "Fire Truck", "Lifeguard Granger",
	"Prison Bus", "Park Ranger Granger", "Police Cruiser", "Police Cruiser 2", "Police Cruiser 3",
	"Unmarked Cruiser", "Police Bike", "Police Cruiser Snow", "Police Cruiser Snow 2", "Police Transporter",
	"Sheriff Cruiser", "Sheriff SUV", "Police Riot",
	"Khanjali", "Thruster", "Chernobog", "Trailersmall2", "Barrage", "APC",
	"Barracks", "HVY Barracks Semi", "Barracks 3", "Canis Crusader", "Rhino Tank",
	"blazer5", "boxville5", "comet3", "diablous", "diablous2",
	"dune4", "dune5", "elegy", "fcr", "fcr2",
	"italigtb", "italigtb2", "nero", "nero2", "penetrator",
	"phantom2", "ruiner2", "ruiner3", "specter", "specter2",
	"technical2","tempesta", "voltic2", "wastelander",
	"BESRA", "CARGOPLANE", "CUBAN800", "DODO", "DUSTER",
	"HYDRA", "JET", "LAZER", "LUXOR", "LUXOR2",
	"MAMMATUS", "MILJET", "NIMBUS", "SHAMAL", "STUNT",
	"TITAN", "VELUM", "VELUM2", "VESTRA", "avenger",
	"tula", "microlight",
	"volatol",
	"BLADE", "BUCCANEER", "BUCCANEER2", "CHINO", "CHINO2",
	"COQUETTE3", "DOMINATOR", "DOMINATOR2", "DUKES", "DUKES2",
	"GAUNTLET", "GAUNTLET2", "FACTION", "FACTION2", "FACTION3",
	"HOTKNIFE", "LURCHER", "MOONBEAM", "MOONBEAM2", "NIGHTSHADE",
	"PHOENIX", "PICADOR", "RATLOADER", "RATLOADER2", "RUINER",
	"SABREGT", "SABREGT2", "SLAMVAN", "SLAMVAN2", "SLAMVAN3",
	"STALION", "STALION2", "TAMPA", "VIGERO", "VIRGO",
	"VIRGO2", "VIRGO3", "VOODOO", "VOODOO2",
	"BFINJECTION", "BIFTA", "BLAZER", "BLAZER2", "BLAZER3",
	"BLAZER4", "BODHI2", "BRAWLER", "DLOADER", "DUBSTA3",
	"DUNE", "DUNE2", "INSURGENT", "INSURGENT2", "KALAHARI",
	"MARSHALL", "MESA3", "MONSTER", "RANCHERXL", "RANCHERXL2",
	"REBEL", "REBEL2", "SANDKING", "SANDKING2", "TECHNICAL",
	"TROPHYTRUCK", "TROPHYTRUCK2",
	"Airport Bus", "MTL Brickade", "Bus", "Dashound", "MTL Dune",
	"Rental Shuttle Bus", "Taxi", "Tourbus", "Trashmaster", "Trashmaster 2",
	"Albany Alpha", "Bravado Banshee", "Grotti Bestia GTS", "Dinka Blista Compact", "Dinka Blista Go GO Monkey",
	"Bravado Buffalo", "Bravado Buffalo S", "Bravado Buffalo Spunk", "CARBONIZZARE", "COMET2",
	"COQUETTE", "ELEGY2", "FELTZER2", "FUROREGT", "FUSILADE",
	"FUTO", "JESTER", "JESTER2", "KHAMELION", "KURUMA",
	"KURUMA2", "LYNX", "MASSACRO", "MASSACRO2", "NINEF",
	"NINEF2", "OMNIS", "PENUMBRA", "RAPIDGT", "RAPIDGT2",
	"RAPTOR", "RUSTON", "SCHAFTER3", "SCHAFTER4", "SCHWARTZER",
	"SEVEN70", "SULTAN", "SURANO", "TAMPA2", "TROPOS",
	"VERLIERER2",
	"BTYPE", "BTYPE2", "BTYPE3", "CASCO", "COQUETTE2",
	"FELTZER3", "INFERNUS2", "JB700", "MAMBA", "MANANA",
	"MONROE", "PEYOTE", "PIGALLE", "STINGER", "STINGERGT",
	"TORNADO", "TORNADO2", "TORNADO3", "TORNADO4", "TORNADO5",
	"TORNADO6", "TURISMO2", "ZTYPE",
	"AIRTUG", "CADDY", "CADDY2", "DOCKTUG", "FORKLIFT",
	"MOWER", "RIPLEY", "SADLER", "SADLER2", "SCRAP",
	"TOWTRUCK", "TOWTRUCK2", "TRACTOR", "TRACTOR2", "TRACTOR3",
	"UTILLITRUCK", "UTILLITRUCK2", "UTILLITRUCK3",
	"ASEA", "ASEA2", "ASTEROPE", "COG55", "COG552",
	"COGNOSCENTI", "COGNOSCENTI2", "EMPEROR", "EMPEROR2", "EMPEROR3",
	"FUGITIVE", "GLENDALE", "INGOT", "INTRUDER", "LIMO2",
	"PREMIER", "PRIMO", "PRIMO2", "REGINA", "ROMERO",
	"SCHAFTER2", "SCHAFTER5", "SCHAFTER6", "STANIER", "STRATUM",
	"STRETCH", "SUPERD", "SURGE", "TAILGATER", "WARRENER",
	"WASHINGTON",
	"AKUMA", "AVARUS", "BAGGER", "BATI", "BATI2",
	"BF400", "CARBONRS", "CHIMERA", "CLIFFHANGER", "DAEMON",
	"DAEMON2", "DEFILER", "DOUBLE", "ENDURO", "ESSKEY",
	"FAGGIO", "FAGGIO2", "FAGGIO3", "GARGOYLE", "HAKUCHOU",
	"HAKUCHOU2", "HEXER", "INNOVATION", "LECTRO", "MANCHEZ",
	"NEMESIS", "NIGHTBLADE", "PCJ", "RATBIKE", "RUFFIAN",
	"SANCHEZ", "SANCHEZ2", "SANCTUS", "SHOTARO", "SOVEREIGN",
	"THRUST", "VADER", "VINDICATOR", "VORTEX", "WOLFSBANE",
	"ZOMBIEA", "ZOMBIEB",
	"Dinghy", "Dinghy 2", "Dinghy 3", "Dinghy 4", "Jetmax",
	"Marquis", "Predator (police)", "Seashark", "Seashark 2", "Seashark 3",
	"Speeder", "Speeder 2", "Squalo", "Submarine", "Submarine 2",
	"Suntrap", "Toro", "Toro 2", "Tropic", "Tropic 2",
	"Tug",
	"ANNIHILATOR", "BLIMP", "BLIMP2", "BUZZARD", "BUZZARD2",
	"CARGOBOB", "CARGOBOB2", "CARGOBOB3", "CARGOBOB4", "FROGGER",
	"FROGGER2", "MAVERICK", "POLMAV", "SAVAGE", "SKYLIFT",
	"SUPERVOLITO", "SUPERVOLITO2", "SWIFT", "SWIFT2", "VALKYRIE",
	"VALKYRIE2", "VOLATUS"
};

const DWORD cars[] =
{
	0xb779a091, 0x25c5af13, 0x9ae6dda1, 0xb1d95da0, 0xb2fe5cf9,	//"ADDER", "BANSHEE2", "BULLET", "CHEETAH", "ENTITYXF",
	0x5502626c, 0x4992196c, 0x30d3f6d8, 0x18f25ac7, 0x767164d6,	//"FMJ", "GP1", "SHEAVA", "INFERNUS", "OSIRIS",
	0xb6846a55, 0x92ef6e04, 0x7e8f677f, 0xdf381e5, 0xee6024bc,	//"LE7B", "PFISTER811", "PROTOTIPO", "REAPER", "SULTANRS",
	0x6322b39a, 0x185484e1, 0x7b406efb, 0x142e0dc3, 0x9f4b77be,	//"T20", "TURISMOR", "TYRUS", "VACCA", "VOLTIC",
	0x7B54A9D3, 0x46699F47, 0xB5EF4C33, 0x1FD824AF, 0x34DBA661,	 //"oppressor2" ,"akula", "vigilante", "Spacedocker", "stromberg",
	0x92EF6E04, 0xD9F0503D,	0x7E8F677F,	0x149BD32A, 0x586765FB, //"pfister811", "scramjet", "prototipo", "PBUS2", "deluxo"
	0x7A2EF5E4, //"rapidgt3",
	0xac5df515, //ZENTORNO
	0x45d56ada, 0x432ea949, 0x9dc66994, 0x73920f8e, 0x1bf8d381,	//"AMBULANCE", "FBI", "FBI2", "FIRETRUK", "LGUARD",
	0x885f3671, 0x2c33b46e, 0x79fbb0c5, 0x9f05f101, 0x71fa16ea,	//"PBUS", "PRANGER", "POLICE", "POLICE2", "POLICE3",
	0x8a63c7b9, 0xfdefaec3, 0xa46462f7, 0x95f4c618, 0x1b38e955,	//"POLICE4", "POLICEB", "POLICEOLD1", "POLICEOLD2", "POLICET",
	0x9baa707c, 0x72935408, 0xb822a1aa,							//"SHERIFF", "SHERIFF2", "RIOT"
	2859440138, 1489874736, 3602674979, 0x8FD54EBB, 4081974053, 562680400,  //"Khanjali", "Thruster", "Chernobog", "Trailersmall2", "Barrage", "APC",
	0xceea3f4b, 0x4008eabb, 0x2592b5cf, 0x132d5a1a, 0x2ea68690,	//"BARRACKS", "BARRACKS2", "BARRACKS3", "CRUSADER", "RHINO"
	0xa1355f67, 0x28ad20e1, 0x877358ad, 0xf1b44f44, 0x6abdf65e,	//"blazer5", "boxville5", "comet3", "diablous", "diablous2",
	0xceb28249, 0xed62bfa9, 0xbba2261, 0x25676eaf, 0xd2d5e00e,	//"dune4", "dune5", "elegy", "fcr", "fcr2",
	0x85e8e76b, 0xe33a477b, 0x3da47243, 0x4131f378, 0x9734f3ea,	//"italigtb", "italigtb2", "nero", "nero2", "penetrator",
	0x9dae1398, 0x381e10bd, 0x2e5afd37, 0x706e2b40, 0x400f5147,	//"phantom2", "ruiner2", "ruiner3", "specter", "specter2",
	0x4662bcbb, 0x1044926f, 0x3af76f4a, 0x8e08ec82,				//"technical2","tempesta", "voltic2", "wastelander"
	0x6cbd1d6d, 0x15f27762, 0xd9927fe3, 0xca495705, 0x39d6779e,	//"BESRA", "CARGOPLANE", "CUBAN800", "DODO", "DUSTER",
	0x39d6e83f, 0x3f119114, 0xb39b0ae6, 0x250b0c5e, 0xb79f589e,	//"HYDRA", "JET", "LAZER", "LUXOR", "LUXOR2",
	0x97e55d11, 0x9d80f93, 0xb2cf7250, 0xb79c1bf5, 0x81794c70,	//"MAMMATUS", "MILJET", "NIMBUS", "SHAMAL", "STUNT",
	0x761e2ad3, 0x9c429b6a, 0x403820e8, 0x4ff77e37, 0x81BD2ED0,
	0x3E2E4F8A, 0x96E24857, 0x1AAD0DED,							//"TITAN", "VELUM", "VELUM2", "VESTRA"
	0xb820ed5e, 0xd756460c, 0xc397f748, 0x14d69010, 0xaed64a63,	//"BLADE", "BUCCANEER", "BUCCANEER2", "CHINO", "CHINO2",
	0x2ec385fe, 0x4ce68ac, 0xc96b73d9, 0x2b26f456, 0xec8f7094,	//"COQUETTE3", "DOMINATOR", "DOMINATOR2", "DUKES", "DUKES2",
	0x94b395c5, 0x14d22159, 0x81a9cddf, 0x95466bdb, 0x866bce26,	//"GAUNTLET", "GAUNTLET2", "FACTION", "FACTION2", "FACTION3",
	0x239e390, 0x7b47a6a7, 0x1f52a43f, 0x710a2b9b, 0x8c2bd0dc,	//"HOTKNIFE", "LURCHER", "MOONBEAM", "MOONBEAM2", "NIGHTSHADE",
	0x831a21d5, 0x59e0fbf3, 0xd83c13ce, 0xdce1d9f7, 0xf26ceff9,	//"PHOENIX", "PICADOR", "RATLOADER", "RATLOADER2", "RUINER",
	0x9b909c94, 0xd4ea603, 0x2b7f9de3, 0x31adbbfc, 0x42bc5e19,	//"SABREGT", "SABREGT2", "SLAMVAN", "SLAMVAN2", "SLAMVAN3",
	0x72a4c31e, 0xe80f67ee, 0x39f9c898, 0xcec6b9b7, 0xe2504942,	//"STALION", "STALION2", "TAMPA", "VIGERO", "VIRGO",
	0xca62927a, 0xfdffb0, 0x779b4f2d, 0x1f3766e3,				//"VIRGO2", "VIRGO3", "VOODOO", "VOODOO2"
	0x432aa566, 0xeb298297, 0x8125bcf9, 0xfd231729, 0xb44f0582,	//"BFINJECTION", "BIFTA", "BLAZER", "BLAZER2", "BLAZER3",
	0xe5ba6858, 0xaa699bb6, 0xa7ce1bc5, 0x698521e3, 0xb6410173,	//"BLAZER4", "BODHI2", "BRAWLER", "DLOADER", "DUBSTA3",
	0x9cf21e0f, 0x1fd824af, 0x9114eada, 0x7b7e56f0, 0x5852838,	//"DUNE", "DUNE2", "INSURGENT", "INSURGENT2", "KALAHARI",
	0x49863e9c, 0x84f42e51, 0xcd93a7db, 0x6210cbb0, 0x7341576b,	//"MARSHALL", "MESA3", "MONSTER", "RANCHERXL", "RANCHERXL2",
	0xb802dd46, 0x8612b64b, 0xb9210fd0, 0x3af8c345, 0x83051506,	//"REBEL", "REBEL2", "SANDKING", "SANDKING2", "TECHNICAL",
	0x612f4b6, 0xd876dbe2,
	0x4c80eb0e, 0xedc6f847, 0xd577c962, 0x84718d34, 0x829a3c44,	//"AIRBUS", "BRICKADE", "BUS", "COACH", "RALLYTRUCK",
	0xbe819c63, 0xc703db5f, 0x73b1c3cb, 0x72435a19, 0xb527915c,	//"RENTALBUS", "TAXI", "TOURBUS", "TRASH", "TRASH2"
	0x2db8d1aa, 0xc1e908d2, 0x4bfcf28b, 0x3dee5eda, 0xdcbc1c3b,	//"ALPHA", "BANSHEE", "BESTIAGTS", "BLISTA2", "BLISTA3",
	0xedd516c6, 0x2bec3cbe, 0xe2c013e, 0x7b8ab45f, 0xc1ae4d16,	//"BUFFALO", "BUFFALO2", "BUFFALO3", "CARBONIZZARE", "COMET2",
	0x67bc037, 0xde3d9d22, 0x8911b9f5, 0xbf1691e0, 0x1dc0ba53,	//"COQUETTE", "ELEGY2", "FELTZER2", "FUROREGT", "FUSILADE",
	0x7836ce2f, 0xb2a716a3, 0xbe0e6126, 0x206d1b68, 0xae2bfe94,	//"FUTO", "JESTER", "JESTER2", "KHAMELION", "KURUMA",
	0x187d938d, 0x1cbdc10b, 0xf77ade32, 0xda5819a3, 0x3d8fa25c,	//"KURUMA2", "LYNX", "MASSACRO", "MASSACRO2", "NINEF",
	0xa8e38b01, 0xd1ad4937, 0xe9805550, 0x8cb29a14, 0x679450af,	//"NINEF2", "OMNIS", "PENUMBRA", "RAPIDGT", "RAPIDGT2",
	0xd7c56d39, 0x2ae524a8, 0xa774b5a6, 0x58cf185c, 0x2b122c82,	//"RAPTOR", "RUSTON", "SCHAFTER3", "SCHAFTER4", "SCHWARTZER",
	0x97398a4b, 0x39da2754, 0x16e478c1, 0xc0240885, 0x707e63a4,	//"SEVEN70", "SULTAN", "SURANO", "TAMPA2", "TROPOS",
	0x41b77fa4,													//"VERLIERER2"
	0x6ff6914, 0xce6b35a4, 0xdc19d101, 0x3822bdfe, 0x3c4e2113,	//"BTYPE", "BTYPE2", "BTYPE3", "CASCO", "COQUETTE2",
	0xa29d6d10, 0xac33179c, 0x3eab5555, 0x9cfffc56, 0x81634188,	//"FELTZER3", "INFERNUS2", "JB700", "MAMBA", "MANANA",
	0xe62b361b, 0x6d19ccbc, 0x404b6381, 0x5c23af9b, 0x82e499fa,	//"MONROE", "PEYOTE", "PIGALLE", "STINGER", "STINGERGT",
	0x1bb290bc, 0x5b42a5c4, 0x690a4153, 0x86cf7cdd, 0x94da98ef,	//"TORNADO", "TORNADO2", "TORNADO3", "TORNADO4", "TORNADO5",
	0xa31cb573, 0xc575df11, 0x2d3bd401,							//"TORNADO6", "TURISMO2", "ZTYPE"
	0x5d0aac8f, 0x44623884, 0xdff0594c, 0xcb44b1ca, 0x58e49664,	//"AIRTUG", "CADDY", "CADDY2", "DOCKTUG", "FORKLIFT",
	0x6a4bd8f6, 0xcd935ef9, 0xdc434e51, 0x2bc345d1, 0x9a9fd3df,	//"MOWER", "RIPLEY", "SADLER", "SADLER2", "SCRAP",
	0xb12314e0, 0xe5a2d6c6, 0x61d6ba8c, 0x843b73de, 0x562a97bd,	//"TOWTRUCK", "TOWTRUCK2", "TRACTOR", "TRACTOR2", "TRACTOR3",
	0x1ed0a534, 0x34e6bf6b, 0x7f2153df,							//"UTILLITRUCK", "UTILLITRUCK2", "UTILLITRUCK3"
	0x94204d89, 0x9441d8d5, 0x8e9254fb, 0x360a438e, 0x29fcd3e4,	//"ASEA", "ASEA2", "ASTEROPE", "COG55", "COG552",
	0x86fe0b60, 0xdbf2d57a, 0xd7278283, 0x8fc3aadc, 0xb5fcf74e,	//"COGNOSCENTI", "COGNOSCENTI2", "EMPEROR", "EMPEROR2", "EMPEROR3",
	0x71cb2ffb, 0x47a6bc1, 0xb3206692, 0x34dd8aa1, 0xf92aec4d,	//"FUGITIVE", "GLENDALE", "INGOT", "INTRUDER", "LIMO2",
	0x8fb66f9b, 0xbb6b404f, 0x86618eda, 0xff22d208, 0x2560b2fc,	//"PREMIER", "PRIMO", "PRIMO2", "REGINA", "ROMERO",
	0xb52b5113, 0xcb0e7cd9, 0x72934be4, 0xa7ede74d, 0x66b4fc45,	//"SCHAFTER2", "SCHAFTER5", "SCHAFTER6", "STANIER", "STRATUM",
	0x8b13f083, 0x42f2ed16, 0x8f0e3594, 0xc3ddfdce, 0x51d83328,	//"STRETCH", "SUPERD", "SURGE", "TAILGATER", "WARRENER",
	0x69f06b57,													//"WASHINGTON"
	0x63abade7, 0x81e38f7f, 0x806b9cc3, 0xf9300cc5, 0xcadd5d2d,	//"AKUMA", "AVARUS", "BAGGER", "BATI", "BATI2",
	0x5283265, 0xabb0c0, 0x675ed7, 0x17420102, 0x77934cee,		//"BF400", "CARBONRS", "CHIMERA", "CLIFFHANGER", "DAEMON",
	0xac4e93c9, 0x30ff0190, 0x9c669788, 0x6882fa73, 0x794cb30c,	//"DAEMON2", "DEFILER", "DOUBLE", "ENDURO", "ESSKEY",
	0x9229e4eb, 0x350d1ab, 0xb328b188, 0x2c2c2324, 0x4b6c568a,	//"FAGGIO", "FAGGIO2", "FAGGIO3", "GARGOYLE", "HAKUCHOU",
	0xf0c2a91f, 0x11f76c14, 0xf683eaca, 0x26321e67, 0xa5325278,	//"HAKUCHOU2", "HEXER", "INNOVATION", "LECTRO", "MANCHEZ",
	0xda288376, 0xa0438767, 0xc9ceaf06, 0x6facdf31, 0xcabd11e8,	//"NEMESIS", "NIGHTBLADE", "PCJ", "RATBIKE", "RUFFIAN",
	0x2ef89e46, 0xa960b13e, 0x58e316c7, 0xe7d2a16e, 0x2c509634,	//"SANCHEZ", "SANCHEZ2", "SANCTUS", "SHOTARO", "SOVEREIGN",
	0x6d6f8f43, 0xf79a00f7, 0xaf599f01, 0xdba9dbfc, 0xdb20a373,	//"THRUST", "VADER", "VINDICATOR", "VORTEX", "WOLFSBANE",
	0xc3d7c72b, 0xde05fb87,										//"ZOMBIEA", "ZOMBIEB"
	0x3d961290, 0x107f392c, 0x1e5e54ea, 0x33b47f96, 0x33581161,	//"DINGHY", "DINGHY2", "DINGHY3", "DINGHY4", "JETMAX",
	0xc1ce1183, 0xe2e7d4ab, 0xc2974024, 0xdb4388e4, 0xed762d49,	//"MARQUIS", "PREDATOR", "SEASHARK", "SEASHARK2", "SEASHARK3",
	0xdc60d2b, 0x1a144f2a, 0x17df5ec2, 0x2dff622f, 0xc07107ee,	//"SPEEDER", "SPEEDER2", "SQUALO", "SUBMERSIBLE", "SUBMERSIBLE2",
	0xef2295c9, 0x3fd5aa2f, 0x362cac6d, 0x1149422f, 0x56590fe9,	//"SUNTRAP", "TORO", "TORO2", "TROPIC", "TROPIC2",
	0x82cac433,
	0x31f0b376, 0xf7004c86, 0xdb6b4924, 0x2f03547b, 0x2c75f0dd,	//"ANNIHILATOR", "BLIMP", "BLIMP2", "BUZZARD", "BUZZARD2",
	0xfcfcb68b, 0x60a7ea10, 0x53174eef, 0x78bc1a3c, 0x2c634fbd,	//"CARGOBOB", "CARGOBOB2", "CARGOBOB3", "CARGOBOB4", "FROGGER",
	0x742e9ac0, 0x9d0450ca, 0x1517d4d9, 0xfb133a17, 0x3e48bf23,	//"FROGGER2", "MAVERICK", "POLMAV", "SAVAGE", "SKYLIFT",
	0x2a54c47d, 0x9c5e5644, 0xebc24df2, 0x4019cb4c, 0xa09e15fd,	//"SUPERVOLITO", "SUPERVOLITO2", "SWIFT", "SWIFT2", "VALKYRIE",
	0x5bfa5c4b, 0x920016f1,										//"VALKYRIE2", "VOLATUS"
};