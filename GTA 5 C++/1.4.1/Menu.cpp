#include <windows.h>
#include <iostream>
#include <vector>
#include "hashes.h"
#include "Menu.h"
#include "Hack.h"
#include "config.h"
#include <conio.h> 
#include "stdafx.h"
#include <TlHelp32.h>

//bool select = false;
int MenuID;
int MenuIDY;
int MenuIteam;
int MenuFunction;

int bx = 500;
int by = 700;

extern int Funktion;
extern int FunktionCar;
extern int FunktionPed;

extern bool CarCarosONOFF;
extern bool AntiNPCONOFF;
extern bool GODMODE;
extern bool FULLGODMODE;
extern bool VGODMODE;
extern bool VFULLGODMODE;
extern bool RPLoopONOFF;
extern bool PadDropONOFF;
extern int i;
//int x = configread(41, 1);
//int y = configread(42, 1);
int x = configread("x");
int y = configread("y");
int MenuSpeed = configread("MenuSpeed(ms)");
int MenuColor = configread("MenuColor(min0max255)");
int devkey = configread("devkey");
extern int	MoneyVal;
extern int switch_on;

int switch_on_dsplay = 0;
int switch_on_klick = 0;

int playerheal;

bool showhide = true;

void TestInfo(__int64 a)
{
	std::cout << std::hex << a << std::endl;
	//TestInfo(adresse);
	//std::string test1 = std::to_string(adresse);
	//MessageBoxA(NULL, test1.c_str(), "testx", MB_OK);
}

void WindowMove() {
	RECT rect;
	POINT lpPoint;
	GetCursorPos(&lpPoint);
	GetWindowRect(GetConsoleWindow(), &rect);

	static bool movewindow;
	int posX = rect.left;
	int posY = rect.top;
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	if (lpPoint.x < x + by && lpPoint.x > x - 1 && lpPoint.y < y + by && lpPoint.y > y - 1)
	{
		if (movewindow == true)
		{
			SetWindowPos(GetConsoleWindow(), 0, x + 258, y, bx, by, SWP_NOZORDER);
			movewindow = false;
		}
	}
	else
	{
		if (movewindow == false)
		{
			SetWindowPos(GetConsoleWindow(), 0, x, y, bx, by, SWP_NOZORDER);
			movewindow = true;
		}
	}
}

void SetMenuColor(int color)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
}

void SetBOTTOM(HWND hWnd)
{
	RECT rect;

	GetWindowRect(hWnd, &rect);

	int posX = rect.left;
	int posY = rect.top;
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	SetWindowPos(GetConsoleWindow(), HWND_TOPMOST, posX, posY, width, height, SWP_HIDEWINDOW);
}

void SetTopMost(HWND hWnd)
{
	RECT rect;

	GetWindowRect(hWnd, &rect);

	int posX = rect.left;
	int posY = rect.top;
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	SetWindowPos(GetConsoleWindow(), HWND_TOPMOST, posX, posY, width, height, SWP_SHOWWINDOW);
}

void MenuShowHide()
{
	if (GetAsyncKeyState(0x6D))
	{

		HWND hConsole = GetConsoleWindow();
		if (showhide == false)
		{
			showhide = true;
			SetTopMost(hConsole);
			Sleep(500);
		}
		else
		{
			showhide = false;
			SetBOTTOM(hConsole);
			Sleep(500);
		}

	}
}


void MenuSetWindow(int posX, int posY, int width, int height, bool Transparent)
{
	HWND console_window = GetConsoleWindow();
	
	if (Transparent == true)
	{
		SetWindowLong(console_window, GWL_EXSTYLE,
	    GetWindowLong(console_window, GWL_EXSTYLE) & !WS_EX_TRANSPARENT & !WS_EX_RIGHTSCROLLBAR);
	}
	
	SetLayeredWindowAttributes(console_window, 0, (bx * 70) / 100, LWA_ALPHA);

	SetWindowPos(console_window,0 ,posX, posY, width, height, SWP_NOZORDER);

	HWND hWnd = GetConsoleWindow();
	RECT rcScr, rcWnd, rcClient;

	GetWindowRect(hWnd, &rcWnd);
	GetWindowRect(GetDesktopWindow(), &rcScr);
	GetClientRect(hWnd, &rcClient);
	//SetWindowLong(hWnd, WS_DLGFRAME, WS_BORDER);
	SetWindowRgn(hWnd, CreateRectRgn(rcClient.left, rcClient.top, rcClient.right, rcClient.bottom), TRUE);
}


void MenuLoadSetWindow()
{


	HWND console_window = GetConsoleWindow();
	SetWindowLong(console_window, GWL_EXSTYLE,
	GetWindowLong(console_window, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_TRANSPARENT | WS_EX_APPWINDOW) | WS_EX_CONTROLPARENT;
	// Make this window 70% alpha 
	SetLayeredWindowAttributes(console_window, 0, (bx * 70) / 100, LWA_ALPHA);



	HWND hWnd = GetConsoleWindow();
	RECT rcScr, rcWnd, rcClient;

	GetWindowRect(hWnd, &rcWnd);
	GetWindowRect(GetDesktopWindow(), &rcScr);
	GetClientRect(hWnd, &rcClient);

	//MoveWindow(hWnd, (rcScr.right / 2) - 330, (rcScr.bottom / 2) - 180, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top, 1);
	SetWindowLong(hWnd, GWL_STYLE, WS_POPUP);
	SetWindowRgn(hWnd, CreateRectRgn(rcClient.left, rcClient.top, rcClient.right, rcClient.bottom), TRUE);
	ShowWindow(hWnd, 1);
	SetMenuColor(MenuColor);
	SetConsoleTextAttribute(console_window, 100);
	MenuSetWindow(x, y, bx, by, 0);
	SetTopMost(hWnd);
}

bool getvalue(int val)
{
	return val;
};

typedef void(*function)(void);
typedef void(*fPTRR)(void);

struct MenuTypen
{
	char ItemText[30];
	function functions;

	bool onoff;
	bool isonoff;

	int setMenuID;
	int setMenuIDY;

	int MenuIDYminlen;
	int MenuIDYmaxlen;
	int MenuIDYSteeps;

	bool MenuIDYon;

	bool BackDisable;

	
	bool SringARRAY;
	const char* const SringARRAYtest[500];
};

const char* const isselect(int i)
{
	if (MenuID == i)
	{
		return("--->");

	}
	return("");
};

void update(int ASIZE, struct MenuTypen itemslist[]) {

	

}

void output(int ASIZE, struct MenuTypen itemslist[])

{
	system("cls");
	for (size_t i = 0; i < ASIZE; i++)
	{
		/*if (itemslist->setMenuID != NULL)
		{
			MenuID = itemslist->setMenuID;
			itemslist->setMenuID = NULL;
		}*/

		if (itemslist[MenuID].MenuIDYon != FALSE && MenuID == i)
		{
			MenuIDY = itemslist[MenuID].setMenuIDY;
			std::cout << isselect(i) << itemslist[i].ItemText << " " <<  "<" << MenuIDY << ">";
		}
		else
		{
			std::cout << isselect(i) << itemslist[i].ItemText;
		}

		if (itemslist[i].isonoff == true)
		{
			if (itemslist[i].onoff == true)
			{
				std::cout << " True";
			}
			else
			{
				std::cout << " False";
			}
		}
		std::cout << "\n";

		if (itemslist[MenuID].SringARRAY != NULL && MenuID == i)
		{
			std::cout << "> " << itemslist[MenuID].SringARRAYtest[MenuIDY] << "\n";
		}
	}
};

void getkeyevent(int MenuIteam, int menuminlen, int menumaxlen, struct MenuTypen itemslist[])
{
	output(menumaxlen, itemslist);
	Sleep(150);
	while (true)
	{
		//std::cout << itemslist->setMenuID;

		if (showhide == true)

		{
			if (GetAsyncKeyState(0x64))
			{
				MenuIDY = itemslist[MenuID].setMenuIDY;
				if (itemslist[MenuID].MenuIDYon != false && MenuIDY != itemslist[MenuID].MenuIDYminlen)
				{
					MenuIDY = MenuIDY - itemslist[MenuID].MenuIDYSteeps;
					itemslist[MenuID].setMenuIDY = MenuIDY;
					Sleep(MenuSpeed); //interval
					output(menumaxlen, itemslist);
				}
			}

			if (GetAsyncKeyState(0x66))
			{
				MenuIDY = itemslist[MenuID].setMenuIDY;
				if (itemslist[MenuID].MenuIDYon != false && MenuIDY != itemslist[MenuID].MenuIDYmaxlen)
				{
					MenuIDY = MenuIDY + itemslist[MenuID].MenuIDYSteeps;
					itemslist[MenuID].setMenuIDY = MenuIDY;
					Sleep(MenuSpeed); //interval
					output(menumaxlen, itemslist);
				}
			}

			if (GetAsyncKeyState(0x68))
			{
				if (MenuID != menuminlen)
				{
					//MenuID = itemslist[MenuID].setMenuIDY;
					MenuID = MenuID - 1;
					Sleep(MenuSpeed); //interval
					output(menumaxlen, itemslist);
					//itemslist[MenuID].setMenuIDY = 0;
				}
			}

			if (GetAsyncKeyState(0x62))
			{
				if (MenuID != menumaxlen - 1)
				{
					//MenuID = itemslist[MenuID].setMenuIDY = 0;
					MenuID = MenuID + 1;
					Sleep(MenuSpeed); //interval
					output(menumaxlen, itemslist);
					//itemslist[MenuID].setMenuIDY = 0;
				}

			}

			if (GetAsyncKeyState(0x65))
			{
				if (itemslist[MenuID].isonoff == true)
				{
					if (itemslist[MenuID].onoff == false)
					{
						itemslist[MenuID].onoff = true;
					}
					else
					{
						itemslist[MenuID].onoff = false;
					}
				}

				itemslist->setMenuID = MenuID;
				if (itemslist[MenuID].functions != NULL)
				{
					//if (itemslist[MenuID].onoff == false)
					//{
					itemslist[MenuID].functions();
					//}
				}
				else
				{
					MenuID = 0;
					if (itemslist[menumaxlen - 1].BackDisable == 1)
					{
						exit(0);
					}
					break;
				}
				//select(MenuID, itemslist); //Select
				Sleep(50); //interval
				output(menumaxlen, itemslist);
			}

			if (GetAsyncKeyState(0x60))
			{
				if (itemslist[menumaxlen - 1].BackDisable == NULL)
				{
					MenuID = 0;
					Sleep(150); //interval
					break;
				}
			}
		}
		//WindowMove();
		MenuShowHide();
		Sleep(50);
	}
}

void Standart()
{

}

void M_Setting_Downt_Work()
{
	std::cout << "This feature dont save.\nYou can change the poiton in the config.";
	Sleep(1000);

}

void M_Settings_Save_X()
{	
	x = MenuIDY;
	MenuSetWindow(x, y, bx, by, 0);
}
void M_Settings_Save_Y()
{
	y = MenuIDY;
	MenuSetWindow(x, y, bx, by, 0);
}
void M_Settings_Save_Menu_Speed()
{
	MenuSpeed = MenuIDY;
}
void M_Settings_Save_Menu_Color()
{
	MenuColor = MenuIDY;
	SetMenuColor(MenuColor);
	MenuSetWindow(x, y, bx, by, 0);
}

struct MenuTypen Settings[] =
{
	{"Position:", Standart},
	{"X",M_Settings_Save_X, false, false, 0, 0, -10000, 10000, 50, true},
	{"Y",M_Settings_Save_Y, false, false, 0, 0, -10000, 10000, 50, true},
	{"Menu Speed(ms)",M_Settings_Save_Menu_Speed, false, false, 0, 25, 0, 1000, 1, true},
	{"Color",M_Settings_Save_Menu_Color, false, false, 0, MenuColor, 1, 255, 1, true },
	//{"Save", M_Settings_Save},
	{"back",NULL }
};

void M_Car_Chaos_Func_Active()
{
	CARC();
}

void M_Car_Chaos_Func()
{
	FunktionCar = MenuID;
}

struct MenuTypen Car_Chaos[] =
{

	{"Car Chaos Active", M_Car_Chaos_Func_Active, false ,true},
	{"Car Chaos", M_Car_Chaos_Func},
	{"Car Fly", M_Car_Chaos_Func},
	{"Car TP to WayPoint",M_Car_Chaos_Func},
	{"Car TP to Players",M_Car_Chaos_Func},
	{"back",NULL }
};

void M_Online_Player_Drop()
{
	SpawnMoneyPlayer();
	MoneyVal = MenuIDY;
}

void M_Online_Player_Ped_Drop()
{
	PedDrop();
	FunktionPed = 2;
	MoneyVal = MenuIDY;
}

void M_Online_Player_Teleport_To_Player()
{
TeleportToPlayer();
}

void M_Online_Player_Car_Chaos_Func()
{
	CARC();
	FunktionCar = 5;
}

void M_Online_Player_Create_Crash_Pickup()
{
	if (devkey == 0)
	{
	SpawnMoneyPlayer();
	Create_Crash_Pickup();
	}
	else
	{
		std::cout << "This feature has been removed." << std::endl << "Pls enter the devkey in the config to unlock it." << std::endl;
		Sleep(1000);
	}
}

struct MenuTypen Online_Player[] =
{
	{"Teleport",M_Online_Player_Teleport_To_Player},
	{"Drop",M_Online_Player_Drop, false, true, 0, 2500, 0, 2500, 100, true},
	{"Ped Drop",M_Online_Player_Ped_Drop, false, true, 0, 2500, 0, 2500, 100, true},
	{"Cars to Player",M_Online_Player_Car_Chaos_Func, false, true},
	{"IP OFF",Standart, false, false},
	{"GodMode",Standart, false, true},
	{"Crash Pickup",M_Online_Player_Create_Crash_Pickup, false, true},
	{"back",NULL }
};


void M_Settings()
{
	MenuID = 0;
	getkeyevent(MenuIteam, 0, (ARRAYSIZE(Settings)), Settings);
}

void M_Car_Chaos()
{
	MenuID = 0;
	getkeyevent(MenuIteam, 0, (ARRAYSIZE(Car_Chaos)), Car_Chaos);
}

void M_Online_Player()
{
	MenuID = 0;
	getkeyevent(MenuIteam, 0, (ARRAYSIZE(Online_Player)), Online_Player);
}

void M_Wanted_Level()
{
	levelchange(MenuIDY);
}

void M_Ped_Drop()
{
	PedDrop();
	FunktionPed = 1;
	MoneyVal = MenuIDY;
}

void M_Spawn_Vehicel()
{
	CREATE_VEHICLE(cars[MenuIDY],0 ,0 ,0 , 1);
	/*	if (devkey == 0)
	{
		SetSpawnV(cars[MenuIDY]);
	}
	else
	{
		std::cout << "This feature has been removed." << std::endl << "Pls enter the devkey in the config to unlock it." << std::endl;
		Sleep(1000);
	}*/
}

void M_Spawn_Pickup()
{
	SpawnMoney(pickups[MenuIDY]);
	MoneyVal = 2500;
	//SetSpawnO(pickups[MenuIDY]);
	//std::cout << "This feature has been removed.";
	//Sleep(1000);
}

void M_Name()
{
	if (devkey == 0)
	{
		name();
	}
	else
	{
		std::cout << "This feature has been removed." << std::endl << "Pls enter the devkey in the config to unlock it." << std::endl;
		Sleep(1000);
	}
}

void M_Player_List()
{
	MenuID = 1;
	HWND console_window = GetConsoleWindow();
	MenuSetWindow( x, y, bx, by*2, 0);
	system("cls");
	TeleportToPlayerList(MenuID);
	Sleep(150); //interval
	while (true)
	{
		Sleep(50);
		if (GetAsyncKeyState(0x68))
		{
			if (MenuID != 1)
			{
				
				MenuID = MenuID - 1;
				Funktion = MenuID;
				system("cls");
				TeleportToPlayerList(MenuID);
				Sleep(MenuSpeed); //interval
			}
		}

		if (GetAsyncKeyState(0x62))
		{
			if (MenuID != 31)
			{
				
				MenuID = MenuID + 1;
				Funktion = MenuID;
				system("cls");
				TeleportToPlayerList(MenuID);
				Sleep(MenuSpeed); //interval
			}
		}

		if (GetAsyncKeyState(0x65))
		{
			MenuSetWindow( x, y, bx, by, 0);
			MenuID = 0;
			getkeyevent(MenuIteam, 0, (ARRAYSIZE(Online_Player)), Online_Player);
			break;
		}

		if (GetAsyncKeyState(0x60))
		{
			MenuSetWindow( x, y, bx, by, 0);
			MenuID = 0;
			Sleep(150); //interval
			break;
		}
	}
}

struct MenuTypen HauptMenu[] =
{
	{"Teleport to Waypoint", Teleport_to_Waypoint},
	{"Teleport to Objective", Teleport_to_Objectiv},
	{"Online Player",M_Player_List},
	{"Godmode",fullgmonoff, false, true},
	{"Demi Godmode",gm, false, true},
	{"Vehicle Godmode",vfullgmonoff, false, true},
	{"Vehicle Demi Godmode",vgm, false, true},
	{"Wanted Level", M_Wanted_Level, false, false, 0, 0, 0, 5, 1, true},
	{"Name Change",M_Name},
	{"RP Loop",RP, false, true},
	{"Ped Drop",M_Ped_Drop, false, true, 0, 2000, 0, 2500, 100, true, false},
	{"Spawn Money",M_Spawn_Pickup, false, true, 0, 0, 0, ARRAYSIZE(pickupsname) - 2, 1, "","","","prop_poly_bag_01", "PICKUP_MONEY_CASE", "PICKUP_MONEY_SECURITY_CASE", "PICKUP_VEHICLE_MONEY_VARIABLE", "PICKUP_WEAPON_MINIGUN",
	"PICKUP_WEAPON_RPG", "PICKUP_WEAPON_MARKSMANRIFLE_MK2", "PICKUP_WEAPON_FIREWORK ", "PICKUP_SUBMARINE", "PICKUP_GANG_ATTACK_MONEY",
	"PICKUP_VEHICLE_HEALTH_STANDARD", "PICKUP_VEHICLE_ARMOUR_STANDARD", "PICKUP_VEHICLE_HEALTH_STANDARD", "PICKUP_MONEY_WALLET", "PICKUP_MONEY_PURSE", "PICKUP_MONEY_DEP_BAG",
	"PICKUP_MONEY_PAPER_BAG ", "PICKUP_HEALTH_STANDARD", "PICKUP_ARMOUR_STANDARD", "PICKUP_PORTABLE_CRATE_UNFIXED", "PICKUP_WEAPON_PROXMINE ", "PistolMk2",
	"PICKUP_WEAPON_ADVANCEDRIFLE",},
	{"Car Chaos",M_Car_Chaos, false, false},
	{"Anti NPC",ANTINPC, false, true},
	{"Spawn Vehicle",M_Spawn_Vehicel, NULL, NULL, 0, 0, 0, ARRAYSIZE(carsname)-1, 1, true,
	"","","ADDER", "BANSHEE2", "BULLET", "CHEETAH", "ENTITYXF",
	"FMJ", "GP1", "SHEAVA", "INFERNUS", "OSIRIS",
	"LE7B", "PFISTER811", "PROTOTIPO", "REAPER", "SULTANRS",
	"T20", "TURISMOR", "TYRUS", "VACCA", "VOLTIC",
	"oppressor2" ,"akula", "vigilante", "Spacedocker", "stromberg",
	"Pfister811", "Scramjet", "Prototipo", "pbus2", "deluxo",
	"rapidgt3",
	"ZENTORNO",
	"Ambulance", "FIB Buffalo", "FIB Granger", "Fire Truck", "Lifeguard Granger",
	"Prison Bus", "Park Ranger Granger", "Police Cruiser", "Police Cruiser 2", "Police Cruiser 3",
	"Unmarked Cruiser", "Police Bike", "Police Cruiser Snow", "Police Cruiser Snow 2", "Police Transporter",
	"Sheriff Cruiser", "Sheriff SUV", "Police Riot",
	"Khanjali", "Thruster", "Chernobog", "Trailersmall2", "Barrage", "APC",
	"Barracks", "HVY Barracks Semi", "Barracks 3", "Canis Crusader", "Rhino Tank",
	"blazer5", "boxville5", "comet3", "diablous", "diablous2",
	"dune4", "dune5", "elegy", "fcr", "fcr2",
	"italigtb", "italigtb2", "nero", "nero2", "penetrator",
	"phantom2", "ruiner2", "ruiner3", "specter", "specter2",
	"technical2","tempesta", "voltic2", "wastelander",
	"BESRA", "CARGOPLANE", "CUBAN800", "DODO", "DUSTER",
	"HYDRA", "JET", "LAZER", "LUXOR", "LUXOR2",
	"MAMMATUS", "MILJET", "NIMBUS", "SHAMAL", "STUNT",
	"TITAN", "VELUM", "VELUM2", "VESTRA", "avenger",
	"tula", "microlight",
	"volatol",
	"BLADE", "BUCCANEER", "BUCCANEER2", "CHINO", "CHINO2",
	"COQUETTE3", "DOMINATOR", "DOMINATOR2", "DUKES", "DUKES2",
	"GAUNTLET", "GAUNTLET2", "FACTION", "FACTION2", "FACTION3",
	"HOTKNIFE", "LURCHER", "MOONBEAM", "MOONBEAM2", "NIGHTSHADE",
	"PHOENIX", "PICADOR", "RATLOADER", "RATLOADER2", "RUINER",
	"SABREGT", "SABREGT2", "SLAMVAN", "SLAMVAN2", "SLAMVAN3",
	"STALION", "STALION2", "TAMPA", "VIGERO", "VIRGO",
	"VIRGO2", "VIRGO3", "VOODOO", "VOODOO2",
	"BFINJECTION", "BIFTA", "BLAZER", "BLAZER2", "BLAZER3",
	"BLAZER4", "BODHI2", "BRAWLER", "DLOADER", "DUBSTA3",
	"DUNE", "DUNE2", "INSURGENT", "INSURGENT2", "KALAHARI",
	"MARSHALL", "MESA3", "MONSTER", "RANCHERXL", "RANCHERXL2",
	"REBEL", "REBEL2", "SANDKING", "SANDKING2", "TECHNICAL",
	"TROPHYTRUCK", "TROPHYTRUCK2",
	"Airport Bus", "MTL Brickade", "Bus", "Dashound", "MTL Dune",
	"Rental Shuttle Bus", "Taxi", "Tourbus", "Trashmaster", "Trashmaster 2",
	"Albany Alpha", "Bravado Banshee", "Grotti Bestia GTS", "Dinka Blista Compact", "Dinka Blista Go GO Monkey",
	"Bravado Buffalo", "Bravado Buffalo S", "Bravado Buffalo Spunk", "CARBONIZZARE", "COMET2",
	"COQUETTE", "ELEGY2", "FELTZER2", "FUROREGT", "FUSILADE",
	"FUTO", "JESTER", "JESTER2", "KHAMELION", "KURUMA",
	"KURUMA2", "LYNX", "MASSACRO", "MASSACRO2", "NINEF",
	"NINEF2", "OMNIS", "PENUMBRA", "RAPIDGT", "RAPIDGT2",
	"RAPTOR", "RUSTON", "SCHAFTER3", "SCHAFTER4", "SCHWARTZER",
	"SEVEN70", "SULTAN", "SURANO", "TAMPA2", "TROPOS",
	"VERLIERER2",
	"BTYPE", "BTYPE2", "BTYPE3", "CASCO", "COQUETTE2",
	"FELTZER3", "INFERNUS2", "JB700", "MAMBA", "MANANA",
	"MONROE", "PEYOTE", "PIGALLE", "STINGER", "STINGERGT",
	"TORNADO", "TORNADO2", "TORNADO3", "TORNADO4", "TORNADO5",
	"TORNADO6", "TURISMO2", "ZTYPE",
	"AIRTUG", "CADDY", "CADDY2", "DOCKTUG", "FORKLIFT",
	"MOWER", "RIPLEY", "SADLER", "SADLER2", "SCRAP",
	"TOWTRUCK", "TOWTRUCK2", "TRACTOR", "TRACTOR2", "TRACTOR3",
	"UTILLITRUCK", "UTILLITRUCK2", "UTILLITRUCK3",
	"ASEA", "ASEA2", "ASTEROPE", "COG55", "COG552",
	"COGNOSCENTI", "COGNOSCENTI2", "EMPEROR", "EMPEROR2", "EMPEROR3",
	"FUGITIVE", "GLENDALE", "INGOT", "INTRUDER", "LIMO2",
	"PREMIER", "PRIMO", "PRIMO2", "REGINA", "ROMERO",
	"SCHAFTER2", "SCHAFTER5", "SCHAFTER6", "STANIER", "STRATUM",
	"STRETCH", "SUPERD", "SURGE", "TAILGATER", "WARRENER",
	"WASHINGTON",
	"AKUMA", "AVARUS", "BAGGER", "BATI", "BATI2",
	"BF400", "CARBONRS", "CHIMERA", "CLIFFHANGER", "DAEMON",
	"DAEMON2", "DEFILER", "DOUBLE", "ENDURO", "ESSKEY",
	"FAGGIO", "FAGGIO2", "FAGGIO3", "GARGOYLE", "HAKUCHOU",
	"HAKUCHOU2", "HEXER", "INNOVATION", "LECTRO", "MANCHEZ",
	"NEMESIS", "NIGHTBLADE", "PCJ", "RATBIKE", "RUFFIAN",
	"SANCHEZ", "SANCHEZ2", "SANCTUS", "SHOTARO", "SOVEREIGN",
	"THRUST", "VADER", "VINDICATOR", "VORTEX", "WOLFSBANE",
	"ZOMBIEA", "ZOMBIEB",
	"Dinghy", "Dinghy 2", "Dinghy 3", "Dinghy 4", "Jetmax",
	"Marquis", "Predator (police)", "Seashark", "Seashark 2", "Seashark 3",
	"Speeder", "Speeder 2", "Squalo", "Submarine", "Submarine 2",
	"Suntrap", "Toro", "Toro 2", "Tropic", "Tropic 2",
	"Tug",
	"ANNIHILATOR", "BLIMP", "BLIMP2", "BUZZARD", "BUZZARD2",
	"CARGOBOB", "CARGOBOB2", "CARGOBOB3", "CARGOBOB4", "FROGGER",
	"FROGGER2", "MAVERICK", "POLMAV", "SAVAGE", "SKYLIFT",
	"SUPERVOLITO", "SUPERVOLITO2", "SWIFT", "SWIFT2", "VALKYRIE",
	"VALKYRIE2", "VOLATUS"},
	{"Settings", M_Settings},
	{"Exit",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1},
	//ItemText,function,onoff,isonoff,setMenuID,setMenuIDY,MenuIDYminlen,MenuIDYmaxlen,MenuIDYSteeps,MenuIDYon,BackDisable;
};

/*const char* const HauptMenuTypen[] =

{
"Teleport to Waypoint", "Teleport to Objective", "Online Player", "Godmode", "Demi Godmode",
"Vehicle Godmode", "Vehicle Demi Godmode",
};*/


/*fPTRR MenuFunctionen[] =

{
	 test , Standart, test3 , test4, test5
};*/

/*void additem(const char* const MenuTypen[32])
{
	for (size_t i = 0; i < sizeof(MenuTypen) / sizeof(MenuTypen[0]); i++)
	{
		std::cout << sizeof(MenuTypen) / sizeof(MenuTypen[0]);
		MenuTypen.ItemText[0] = MenuTypen[0];
		MenuTypen.functions = MenuFunctionen;
	}
}*/

/*struct Menutest
{
	const char* const ItemText[by];
} Menutest;


struct Menutest Menusub[] =
{
	"PICKUP_MONEY_SECURITY_CASE", "PICKUP_MONEY_CASE", "PICKUP_MONEY_SECURITY_CASE", "PICKUP_VEHICLE_MONEY_VARIABLE", "PICKUP_WEAPON_MINIGUN",
	"PICKUP_WEAPON_RPG", "PICKUP_WEAPON_MARKSMANRIFLE_MK2", "PICKUP_WEAPON_FIREWORK ", "PICKUP_SUBMARINE", "PICKUP_GANG_ATTACK_MONEY",
	"PICKUP_VEHICLE_ARMOUR_STANDARD", "PICKUP_VEHICLE_ARMOUR_STANDARD", "PICKUP_VEHICLE_HEALTH_STANDARD", "PICKUP_MONEY_WALLET", "PICKUP_MONEY_PURSE", "PICKUP_MONEY_DEP_BAG",
	"PICKUP_MONEY_PAPER_BAG ", "PICKUP_HEALTH_STANDARD", "PICKUP_ARMOUR_STANDARD", "PICKUP_PORTABLE_CRATE_UNFIXED", "PICKUP_WEAPON_PROXMINE ",
	"PICKUP_WEAPON_ADVANCEDRIFLE"
};


void test3(struct MenuTypen itemslist[])
{
	Menutest.ItemText[i];
	itemslist[MenuID].functions();
}*/



void MenuLoad()
{
	MenuLoadSetWindow();
	output(ARRAYSIZE(HauptMenu), HauptMenu);
	getkeyevent(MenuIteam, 0 , (ARRAYSIZE(HauptMenu)),HauptMenu);
	Sleep(50);
}