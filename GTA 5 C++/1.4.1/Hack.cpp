//#pragma once
#include "stdafx.h"
#include "Memory.h"
#include "patternscan.h"
#include "Menu.h"
#include "Hack.h"
#include <conio.h> 
#include <windows.h>
#include <iostream>
#include <vector>
#include <TlHelp32.h>
#include "Offsets.h"
#include "config.h"
DWORD_PTR PTRModelHash;
DWORD_PTR FindModelPTR;
DWORD_PTR GlobalPTR;
DWORD_PTR World;
DWORD_PTR Waypoint;
DWORD_PTR Objective;
DWORD_PTR ReplayInterfaceAddress;
DWORD_PTR AmmoAddress;
DWORD_PTR ClipAddress;
DWORD_PTR BlipAddress;
DWORD_PTR NameAddress;
DWORD_PTR PlayerListAddress;
DWORD_PTR PlayerPOOL = 0x2BD0AA0;
DWORD_PTR NPCPOOL = 0x2351308;
DWORD_PTR CarPOOL = 0x0222BBA0;
int value4 = 1137499272;

extern int Funktion;
extern int FunktionCar;
extern int FunktionPed;

extern bool CarCarosONOFF;
extern bool AntiNPCONOFF;
extern bool GODMODE;
extern bool FULLGODMODE;
extern bool VGODMODE;
extern bool VFULLGODMODE;
extern bool RPLoopONOFF;
extern bool PadDropONOFF;

extern int i;

extern int	MoneyVal;
extern int switch_on;

struct playerdata
{
	DWORD64 PlayerAdresse;
	struct vector3 vec3;
	bool godmode;
	std::string ipadress;
	std::string name;
};

bool SpawnMoneyONOFF;
bool SpawnMoneyPlayerONOFF;
int SpawnMoneypickup;
bool RainbowColorsONOFF;
__int64 value = 0;
int flyg2 = 1091025877;
//int flyg2 = 4000000000;
int address8 = 0x01F81C10;
unsigned int counter = 0x0;
__int64 carcounter = 0x0;
unsigned int NPCcounter = 0x0;
bool Bypass = configread("BypassActive");
bool BypassActive = 0;

__int64 Base = 0;
HANDLE phandle;

DWORD64 GetWorld()
{
	return ReadMemory<DWORD64>(phandle, (Base + World));
}

DWORD64 GetPlayer()
{
	return ReadMemory<DWORD64>(phandle, GetWorld() + OFFSET_PLAYER);
}

DWORD64 GetVehicle()
{
	return ReadMemory<DWORD64>(phandle, GetPlayer() + OFFSET_PLAYER_VEHICLE);
}

DWORD64 GetPosBase(DWORD64 entity)
{
	return  ReadMemory<DWORD64>(phandle, entity + OFFSET_ENTITY_POSBASE);
}

vector3 GetPos(DWORD64  entity)
{
	return ReadMemory<vector3>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS);
}

float GetPosX(DWORD64  entity)
{
	return ReadMemory<float>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS);
}

float GetPosY(DWORD64  entity)
{
	return ReadMemory<float>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS + 0x4);
}

float GetPosZ(DWORD64  entity)
{
	return ReadMemory<float>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS + 0x8);
}

void SetPos(DWORD64 entity, vector3 value)
{
	WriteMemory<vector3>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS, value);
	WriteMemory<vector3>(phandle, entity + OFFSET_ENTITY_POS, value);
}

void SetPosX(DWORD64 entity, float value)
{
	WriteMemory<float>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS, value);
	WriteMemory<float>(phandle, entity + OFFSET_ENTITY_POS, value);
}

void SetPosY(DWORD64 entity, float value)
{
	WriteMemory<float>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS + 0x4, value);
	WriteMemory<float>(phandle, entity + OFFSET_ENTITY_POS + 0x4, value);
}

void SetPosZ(DWORD64 entity, float value)
{
	WriteMemory<float>(phandle, GetPosBase(entity) + OFFSET_ENTITY_POSBASE_POS + 0x8, value);
	WriteMemory<float>(phandle, entity + OFFSET_ENTITY_POS + 0x8, value);
}

int GetHealth(DWORD64 entity)
{
	return ReadMemory<float>(phandle, entity + OFFSET_ENTITY_HEALTH);
}

void SetHealth(DWORD64 entity, DWORD value)
{
	WriteMemory<float>(phandle, entity + OFFSET_ENTITY_HEALTH, value);
}

float GetHealthMax(DWORD64 entity)
{
	return ReadMemory<float>(phandle, entity + OFFSET_ENTITY_HEALTH_MAX);
}

int GetWantedLevel(DWORD64 entity)
{
	return ReadMemory<int>(phandle, entity + OFFSET_PLAYER_INFO_WANTED_CAN_CHANGE);
}

void SetWantedLevel(DWORD64 entity, DWORD value)
{
	WriteMemory<int>(phandle, entity + OFFSET_PLAYER_INFO_WANTED_CAN_CHANGE, value);
}

int GetGodmode()
{
	return ReadMemoryOffsets<BYTE>(phandle, Base + World, { 0x8, 0x189 });
}

BYTE InVehicle()
{
	std::vector<unsigned int> InCarOffset = { 0x8,0x1452 };
	return ReadMemoryOffsets<BYTE>(phandle, (Base + World), InCarOffset);
}

void fullgm(BYTE setONOFF)
{
	WriteMemoryOffsets<BYTE>(phandle, Base + World, setONOFF, { 0x8, 0x189 });
}

void vfullgm(BYTE setONOFF)
{
	if (InVehicle() == 0x0)
	{
		WriteMemoryOffsets<BYTE>(phandle, Base + World, setONOFF, { 0x8, 0xD28, 0x189 });
	}
}

void levelchange(int setlevel)
{
	WriteMemoryOffsets<int>(phandle, Base + World, setlevel, { 0x8, 0x10B8, 0x848 });
}

int clocktimer(int timercount, int timermax)
{
	timercount = timercount +1;
	Sleep(1);
	if (timercount == timermax)
	{
		timercount = 0;
	}
	return timercount;
}
int GetModelHash()
{
	std::vector<unsigned int> ModelHashOffsets = { 0x0,0x2640 };
	return ReadMemoryOffsets<int>(phandle, (Base + PTRModelHash), ModelHashOffsets);
}

void SetModelHash(int value)//work
{
	std::vector<unsigned int> ModelHashOffsets = { 0x0,0x2640 };
	WriteMemoryOffsets<int>(phandle, (Base + PTRModelHash), value, ModelHashOffsets);
}

void SetBaypassModelHash(int value)//work
{
	std::vector<unsigned int> ModelHashOffsets = { 0x0,0x2640 };
	WriteMemoryOffsets<int>(phandle, (Base + PTRModelHash), value, ModelHashOffsets);
}

int lastmodelmax = 0;
void Update_Model()//dont work
{
	if (0x9CA6F755 != GetModelHash())
	{
		SetModelHash(0x9CA6F755);
	}
	std::vector<unsigned int> MoneyPickupMax = { 0x20,0x110 };
	if (lastmodelmax < (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, MoneyPickupMax)))
	{
		Sleep(100);
		SetModelHash(0xEE5EBC97);
	}
	lastmodelmax = (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, MoneyPickupMax));
}

void SetmodelPTRsearch(unsigned int model, unsigned int model2)//work
{
	DWORD64 ModelPTR = ReadMemoryOffsets<DWORD64>(phandle, (Base + FindModelPTR), { model });
	WriteMemoryOffsets<DWORD64>(phandle ,(Base + FindModelPTR), ModelPTR,{ model2 });
}

unsigned int GetmodelPTRsearch(int model)//work
{
	for (unsigned int i = 0; i < 0x10000; i = i + 0x8)
	{

		if (model == (int)ReadMemoryOffsets<int>(phandle, (Base + FindModelPTR), { i , 0x18 }))
		{
			return i;
		}
	}
}

DWORD64 modelsearch(int modelhash)
{
	std::vector<unsigned int> PickupModelOffset = { 0x20, 0x18 };
	for (unsigned int i = 0; i < 0x1000000; i = i + 0x8)
	{
		if (modelhash == (int)ReadMemoryOffsets<int>(phandle, (Base + FindModelPTR), { i, 0x18 }))
		{
			return (ReadMemoryOffsets<DWORD64>(phandle, (Base + FindModelPTR), { i }));
		}
	}
}

int lastmodel = 0xEE5EBC97;
DWORD64 pickupmodel = 0;
void SetPickup(int pickuphash, int modelhash)//, int Money, int x, int y, int z)
{
	int pickupmax;
	DWORD64 pickups;

	std::vector<unsigned int> MoneyPickupMax = { 0x20,0x110 };
	std::vector<unsigned int> MoneyPickup = { 0x20,0x100 };
	std::vector<unsigned int> pos = { 0x30,0x50 };
	std::vector<unsigned int> PickupModelOffset = { 0x18 };
	//std::cout << std::hex << modelsearch(0xEE5EBC97) << std::endl;
	pickupmax = (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, MoneyPickupMax) - 1) * 0x10;

	if (BypassActive == 0)
	{
		if (Bypass == 1)
		{
			if (0x99900 > GetmodelPTRsearch(lastmodel))
			{
				pickupmodel = (GetmodelPTRsearch(lastmodel));
				SetmodelPTRsearch(GetmodelPTRsearch(modelhash), GetmodelPTRsearch(lastmodel));
				BypassActive = 1;
				lastmodel = modelhash;
			}
			else
			{
				BypassActive = 2;
			}
		}
	}
	else
	{
		if (lastmodel != modelhash && BypassActive != 2)
		{
			SetmodelPTRsearch(GetmodelPTRsearch(modelhash), pickupmodel);
		}
	}

	for (int i = 0; i < pickupmax; i = i + 0x10)
	{
		pickupmax = (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, MoneyPickupMax) - 1) * 0x10;
		pickups = ReadMemoryOffsets<DWORD64>(phandle, Base + ReplayInterfaceAddress, MoneyPickup);
		pickups = ReadMemory<DWORD64>(phandle, pickups + i);

		WriteMemory<int>(phandle, pickups + 0x488, pickuphash);
		WriteMemory<int>(phandle, pickups + 0x13A0, MoneyVal);
		if (BypassActive == 0 && modelhash != 0 && lastmodel == (int)ReadMemoryOffsets<DWORD64>(phandle, pickups + 0x20, { PickupModelOffset }))
		{
			WriteMemory<DWORD64>(phandle, pickups + 0x20, modelsearch(modelhash));
		}
		//WriteMemoryOffsets<vector3>(phandle, (pickups), pos, pos);
		//WriteMemoryOffsets<int>(phandle, pickups + 0x20, modelhash, PickupModelOffset);
	}
}

void Create_Crash_Pickup() //playerdatainfo.vec3
{
	int Pickup = 3732468094;
	int Model = -1332461625;
	DWORD64 pickupmodel;
	int pickupmax;
	DWORD64 pickups;
	//int lastmodel = 0xEE5EBC97;
	int lastmodel = 0x9CA6F755;

	std::vector<unsigned int> MoneyPickupMax = { 0x20,0x110 };
	std::vector<unsigned int> MoneyPickup = { 0x20,0x100 };
	std::vector<unsigned int> xOffest = { 0x30,0x50 };
	std::vector<unsigned int> yOffest = { 0x30,0x54 };
	std::vector<unsigned int> zOffest = { 0x30,0x58 };
	std::vector<unsigned int> PickupModelOffset = { 0x18 };
	//std::cout << std::hex << modelsearch(0xEE5EBC97) << std::endl;
	pickupmax = (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, MoneyPickupMax) - 1) * 0x10;
	for (int i = 0; i < pickupmax; i = i + 0x10)
	{
		pickupmax = (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, MoneyPickupMax) - 1) * 0x10;
		pickups = ReadMemoryOffsets<DWORD64>(phandle, Base + ReplayInterfaceAddress, MoneyPickup);
		pickups = ReadMemory<DWORD64>(phandle, pickups + i);

		WriteMemory<int>(phandle, pickups + 0x488, Pickup);
		WriteMemory<int>(phandle, pickups + 0x13A0, MoneyVal);
		if (Model != 0 && lastmodel == (int)ReadMemoryOffsets<DWORD64>(phandle, pickups + 0x20, { PickupModelOffset }))
		{
			WriteMemory<DWORD64>(phandle, pickups + 0x20, modelsearch(Model));
		}

		//WriteMemoryOffsets(phandle, (pickups), x, xOffest);
		//WriteMemoryOffsets(phandle, (pickups), y, yOffest);
		//WriteMemoryOffsets(phandle, (pickups), z, zOffest);
		//WriteMemoryOffsets(phandle, pickups + 0x20, 0x9CA6F755, PickupModelOffset);
	}
}

DWORD64 _GetGlobal(int AtIndex)
{

	return ReadMemory<DWORD64>(phandle, (Base + GlobalPTR) + (8 * (AtIndex >> 0x12 & 0x3F))) + (8 * (AtIndex & 0x3FFFF));

	//return ReadMemory<DWORD64>(phandle, (ReadMemory<DWORD64>(phandle, (Base + GlobalPTR)) + (8 * (AtIndex >> 0x12 & 0x3F))) + (8 * (AtIndex & 0x3FFFF)));
}

void SGV()
{
	while (true)
	{
	DWORD64 value = 0;
	std::cin >> std::hex >> value;
	for (size_t i = 0; i < 5000000; i++)
	{
		if (_GetGlobal(i) == value)
		{
			std::cout << i;
		}
	}
	}
}

void tune_vehicle()
{
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 6), 255);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 5), 255);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 27), 1);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 28), 1);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 29), 1);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 30), 1);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 31), 1);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 70), 1);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 74), 255);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 75), 255);
	WriteMemory<BYTE>(phandle, _GetGlobal(2459034 + 27 + 76), 255);
	WriteMemory<int>(phandle, _GetGlobal(2459034 + 27 + 77), 0xF0400000);
}

//void CREATE_VEHICLE(int modelhash, vector3 pos, bool tune)
void CREATE_VEHICLE(int modelhash, float x, float y, float z, bool tune)
{
	WriteMemory<int>(phandle, _GetGlobal(2459034 + 2), 1);
	WriteMemory<byte>(phandle, _GetGlobal(2459034 + 5), 1);
//--00	WriteMemory<byte>(phandle, _GetGlobal(2459034 + 6), 0);
	WriteMemory<int>(phandle, _GetGlobal(2459034 + 27 + 66), modelhash);
	WriteMemory<float>(phandle, _GetGlobal(2459034 + 7), GetPosX(GetPlayer()) + 5);
	WriteMemory<float>(phandle, _GetGlobal(2459034 + 7+1), GetPosY(GetPlayer()) + 5);
	WriteMemory<float>(phandle, _GetGlobal(2459034 + 7+2), -255);
	if (tune == 1)
	{
		tune_vehicle();
	}
}
//void CREATE_AMBIENT_PICKUP(int pickuphash, vector3 pos, int money, int modelhash)
void CREATE_AMBIENT_PICKUP(int pickuphash, float x, float y, float z, int money, int modelhash)
{
	WriteMemory<byte>(phandle,  _GetGlobal(2507706),1);
	WriteMemory<int>(phandle, _GetGlobal(2507700 + 1), money);
	WriteMemory<float>(phandle, _GetGlobal(2507700 + 3), x);
	WriteMemory<float>(phandle, _GetGlobal(2507700 + 4), y);
	WriteMemory<float>(phandle, _GetGlobal(2507700 + 5), z);
	WriteMemory<byte>(phandle, _GetGlobal(4263810 + 1 + (ReadMemory<int>(phandle, _GetGlobal(2507700)) * 84) + 65 + 2), 2);	
	SetPickup(pickuphash, modelhash);
}

void Teleportcartome()
{
	SetPos(GetPlayer(),GetPos(GetVehicle()));
}

playerdata playerliste(unsigned int Select)
{
	DWORD64 Adresse;
	i = i + 0x8;
	if (i > 0x260)
	{
		i = 0x170;
	}

	playerdata playerdatainfo;

	//std::vector<unsigned int> PlayerAdressOffset = { 0x178 + (select * 0x8) };
	//playerdatainfo.PlayerAdresse = ReadMemoryOffsets<DWORD64>(phandle, Base + PlayerListAddress, PlayerAdressOffset);
	
	Adresse = ReadMemory<DWORD64>(phandle, Base + PlayerListAddress);
	Adresse += 0x178 + (i);

	std::vector<unsigned int> PlayerNameOffset = { 0xA8,0x7C };
	playerdatainfo.name = ReadMemoryOffsets<DWORD64>(phandle, playerdatainfo.PlayerAdresse, PlayerNameOffset);

	std::vector<unsigned int> PlayerPosOffset = { 0xA8,0x1C8,0x30,0x50 };
	playerdatainfo.vec3 = ReadMemoryOffsets<vector3>(phandle, playerdatainfo.PlayerAdresse, PlayerPosOffset);

	std::vector<unsigned int> PlayerAdresseOffset = { 0xA8,0x1C8};
	playerdatainfo.PlayerAdresse = ReadMemoryOffsets<DWORD64>(phandle, playerdatainfo.PlayerAdresse, PlayerPosOffset);

	std::vector<unsigned int> PlayerGodmodeOffset = { 0xA8,0x1C8,0x189 };
	playerdatainfo.godmode = ReadMemoryOffsets<DWORD64>(phandle, playerdatainfo.PlayerAdresse, PlayerPosOffset);

	return playerdatainfo;
}

std::string ConvertToString(DWORD64 value)
{
	std::stringstream ss;
	ss << value;
	return ss.str();
}

playerdata playerselect(unsigned int Select)
{

	char PlayerName[25] = "";
	//std::string PlayerName;
	playerdata playerdatainfo;
	DWORD64 Adresse;
	DWORD64 AdresseName;

	std::vector<unsigned int> PlayerAdressOffset = { 0x178 + (Select *0x8) };
	Adresse = ReadMemory<DWORD64>(phandle, Base + PlayerListAddress);
	Adresse += 0x178 + (Select * 0x8);
	
	//std::vector<unsigned int> PlayerNameOffset = { 0xA8 0x7C};
	//AdresseName = ReadMemoryOffsets<DWORD64>(phandle, Adresse, PlayerNameOffset);
	ReadProcessMemory(phandle, (void*)Adresse, &value, sizeof(value), 0);
	value += 0xA8;
	ReadProcessMemory(phandle, (void*)value, &value, sizeof(value), 0);
	value += 0x7C;
	ReadProcessMemory(phandle, (void*)value, &PlayerName, sizeof(PlayerName), 0);
	playerdatainfo.name = PlayerName;

	std::vector<unsigned int> PlayerPosOffset = { 0xA8,0x1C8,0x30,0x50 };
	playerdatainfo.vec3 = ReadMemoryOffsets<vector3>(phandle, Adresse, PlayerPosOffset);

	std::vector<unsigned int> PlayerAdresseOffset = { 0xA8,0x1C8 };
	playerdatainfo.PlayerAdresse = ReadMemoryOffsets<DWORD64>(phandle, Adresse, PlayerPosOffset);

	std::vector<unsigned int> PlayerGodmodeOffset = { 0xA8,0x1C8,0x189 };
	playerdatainfo.godmode = ReadMemoryOffsets<DWORD64>(phandle, Adresse, PlayerPosOffset);

	return playerdatainfo;
}

void TeleportToPlayerList(int MenuID)
{
	//char PlayerNameTemp = 0;
	std::string PlayerNameTemp;
	const char *y;
	for (int x = 1; x <= 31; x++)
	{
		playerdata playerdatainfo = playerselect(x);

		if (MenuID == x)
		{
			y = "--->";
		}
		else
		{
			y = "";
		}

		if (PlayerNameTemp == playerdatainfo.name)

		{
			std::cout << y << x << "	" << "No Player" << "\n";
		}
		else
		{
			std::cout << y << x << "	" << playerdatainfo.name << "\n";
		}
		PlayerNameTemp = playerdatainfo.name;
	}
}

void TeleportToPlayer()
{
	static int a;

	playerdata playerdatainfo = playerselect(Funktion);
	std::cout << "Teleport	" << playerdatainfo.name << "\n";


		playerdatainfo = playerselect(Funktion);

		if (playerdatainfo.vec3.z != float(-50.000000) && playerdatainfo.vec3.z != float(-180.000000))
		{
			SetPos(GetPlayer(), playerdatainfo.vec3);
			if (InVehicle() == 0x0)
			{
				SetPos(GetVehicle(), playerdatainfo.vec3);
			}
		}
		else
		{
			SetPos(GetPlayer(), playerdatainfo.vec3);
			if (InVehicle() == 0x0)
			{
				SetPos(GetVehicle(), playerdatainfo.vec3);
			}
			Sleep(2000);
			playerdatainfo = playerselect(Funktion);
			SetPos(GetPlayer(), playerdatainfo.vec3);
			if (InVehicle() == 0x0)
			{
				SetPos(GetVehicle(), playerdatainfo.vec3);
			}
			if (playerdatainfo.vec3.z == float(-50.000000) || playerdatainfo.vec3.z == float(-180.000000))
			{
				playerdatainfo.vec3.z = -255;
				SetPos(GetPlayer(), playerdatainfo.vec3);
				if (InVehicle() == 0x0)
				{
					SetPos(GetVehicle(), playerdatainfo.vec3);
				}
			}
		}
}

void Teleport(vector3 Pos)
{
	std::vector<unsigned int> PlayerPos = { 0x8,0x30,0x50 };
	std::vector<unsigned int> PlayerPosSee = { 0x8,0x90 };
	if (Pos.z == 1)
	{
		Pos.z = -255;
	}

	if (Pos.x != 0 && Pos.y != 0)
	{
	SetPos(GetPlayer(), Pos);

	if (InVehicle() == 0x0)
	{
		SetPos(GetVehicle(), Pos);
		WriteMemoryOffsets<vector3>(phandle, (Base + World), Pos, { 0x8, 0xD28, 0x30, 0x50 });
		WriteMemoryOffsets<vector3>(phandle, (Base + World), Pos, { 0x8, 0xD28, 0x90 });
	}
	}
}

vector3 get_blip_waypoint()
{
	static __int64 Adressblip;
	static vector3 blip;
	static int ColorWaypoint = 84;
	static int SpriteWaypoint = 8;
	static int dwColor;
	static int iIcon;
	for (int i = 0; i < 0x2500; i = i + 0x8)
	{
		ReadProcessMemory(phandle, (void*)(Base + BlipAddress + i), &Adressblip, sizeof(Adressblip), 0);
		ReadProcessMemory(phandle, (void*)(Adressblip + 0x48), &dwColor, sizeof(dwColor), 0);
		ReadProcessMemory(phandle, (void*)(Adressblip + 0x40), &iIcon, sizeof(iIcon), 0);

		if (dwColor == ColorWaypoint || iIcon == SpriteWaypoint)
		{
			break;
		}
		else
		{
			blip.x = 0; blip.y = 0; blip.z = 0;
		}
	}
	ReadProcessMemory(phandle, (void*)(Adressblip + 0x10), &blip, sizeof(blip), 0);
	return blip;
}

vector3 get_blip_Objectiv()
{
	static __int64 Adressblip;
	static vector3 blip;
	static int ColorYellowMission = 66;
	static int ColorYellow = 5;
	static int ColorWhite = 0;
	static int ColorGreen = 2;
	static int SpriteCrateDrop = 306;
	static int SpriteStandard = 1;
	static int SpriteRaceFinish = 38;
	static int dwColor;
	static int iIcon;
	for (int i = 0; i < 0x2500; i = i + 0x8)
	{
		ReadProcessMemory(phandle, (void*)(Base + BlipAddress + i), &Adressblip, sizeof(Adressblip), 0);
		ReadProcessMemory(phandle, (void*)(Adressblip + 0x48), &dwColor, sizeof(dwColor), 0);
		ReadProcessMemory(phandle, (void*)(Adressblip + 0x40), &iIcon, sizeof(iIcon), 0);
		if (
			dwColor == ColorYellowMission || iIcon == SpriteStandard
			&& dwColor == ColorYellow || iIcon == SpriteStandard
			&& dwColor == ColorWhite || iIcon == SpriteRaceFinish
			&& dwColor == ColorGreen || iIcon == SpriteStandard
			&& dwColor == SpriteCrateDrop)
		{
			break;
		}
		else
		{
			blip.x = 0; blip.y = 0; blip.z = 0;
		}
	}
	ReadProcessMemory(phandle, (void*)(Adressblip + 0x10), &blip, sizeof(blip), 0);
	return blip;
}

void Teleport_to_Waypoint()
{
	static vector3 blip;
	blip = get_blip_waypoint();
	if (blip.x != 0 && blip.y != 0)
	{
		Teleport(blip);
	}

}

void Teleport_to_Objectiv()
{
	static vector3 blip;
	blip = get_blip_Objectiv();
	if (blip.x != 0 && blip.y != 0)
	{
		Teleport(blip);
	}

}

void name()
{
	char Name[17] = "";
	int NameSearch;
	int found = 0;
	std::cin >> Name;

	std::vector<unsigned int> NameAddressOffset = { 0x5C };
	std::vector<unsigned int> PlayerNameOffset = { 0x8,0x10B8,0x7C };

	NameSearch = ReadMemoryOffsets<int>(phandle, Base + NameAddress, NameAddressOffset);

	//strlen(NameSearch)
	auto buffer = new int[SIZE_ + 1];
	memset(buffer, 0, SIZE_ + 1);
	ReadProcessMemory(phandle, (LPVOID)Base, buffer, SIZE_, 0);
	for (i = 0x0000000; i < SIZE_; i++)
	{
		if (buffer[i] == NameSearch)
		{
			////WriteMemory<std::string>(phandle, Base + i * 4, Name);
			found++;
		    if (found == 3)
			{
				//WwriteMemory<std::string>(phandle, Base + i * 4, Name);
				WriteProcessMemory(phandle, (void*)(Base + i * 4), &Name, sizeof(Name), 0);
				break;
			}
			continue;
		}
	}

	delete[] buffer;
}

void ONOFF(bool *a, char* b)
{
	if (*a == 0)
	{
		*a = 1;
		Sleep(500);
		std::cout << b << " On" << std::endl;
	}
	else
	{
		*a = 0;
		Sleep(500);
		std::cout << b << " Off" << std::endl;
	}
}

void fullgmonoff()
{

	if (FULLGODMODE == 0)
	{
		FULLGODMODE = 1;
		fullgm(0x1);
		Sleep(500);
		std::cout << "GODMODE ON" << "\n";
	}
	else
	{
		FULLGODMODE = 0;
		fullgm(0x0);
		Sleep(500);
		std::cout << "GODMODE OFF" << "\n";
	}
}

void vfullgmonoff()
{

	if (VFULLGODMODE == 0)
	{
		VFULLGODMODE = 1;
		vfullgm(0x1);
		Sleep(500);
		std::cout << "VFULLGODMODE ON" << "\n";
	}
	else
	{
		VFULLGODMODE = 0;
		vfullgm(0x0);
		Sleep(500);
		std::cout << "VFULLGODMODE OFF" << "\n";
	}
}


void vgm()
{
	ONOFF(&VGODMODE, "VGODMODE");
}

void gm()
{
	ONOFF(&GODMODE, "GODMODE");
}

void SpawnMoney(int pickuphash)
{
	if (SpawnMoneyONOFF == 0)
	{
		SpawnMoneyONOFF = 1;
		Sleep(500);
		std::cout << "Spawn Money ON" << "\n";
		SpawnMoneypickup = pickuphash;
	}
	else
	{
		SpawnMoneyONOFF = 0;
		Sleep(500);
		std::cout << "Spawn Money OFF" << "\n";
	}
}

void RainbowColors()
{
	ONOFF(&RainbowColorsONOFF, "RainbowColors");
}

void PedDrop()
{
	ONOFF(&PadDropONOFF, "Ped Drop");
}

void RP()
{
	ONOFF(&RPLoopONOFF, "RP Loop");
}

void SpawnMoneyPlayer()
{
	ONOFF(&SpawnMoneyPlayerONOFF, "Spawn Money");
}

void CARC()
{
	ONOFF(&CarCarosONOFF, "Car Caros");
}

void ANTINPC()
{
	ONOFF(&AntiNPCONOFF, "Anti NCP");
}

DWORD pid;
HWND hwnd;


void MemoryLoad()
{
	//wchar_t* FensterName = L"Grand Theft Auto V";
	hwnd = FindWindow(NULL, L"Grand Theft Auto V");
	Base = GetModulBaseAddr64(L"GTA5.exe", L"GTA5.exe");

	GetWindowThreadProcessId(hwnd, &pid);
	phandle = OpenProcess(PROCESS_ALL_ACCESS, 0, pid);

	SetForegroundWindow(hwnd);
	//HANDLE BASE = OpenProcess(PROCESS_VM_READ, 0, pid);	

		if (!hwnd || Base == 0)
		{
			std::cout << "Window not found!\n";
			std::cout << "Pls run GTA 5 and go in the singelplayer.\n";
			std::cout << "Run the exe as admin.\n";
			std::cin.get();
		}
		else
		{
			std::cout << "Extern Menu Version 1.4.1 Open Source by External Memory" << "\n";
			PTRModelHash = PointerScanPatternEx(phandle, Base, SIZE_, "\x48\x8B\x0D\xCC", "xxxx");//0x024A2C70
			//PTRModelHash = PointerScanPatternEx(phandle, Base, SIZE_, "\x48\x8B\x0D\xCC\xE2", "xxxxx");//0x024A2C70
			//FindModelPTR = PointerScanPatternEx(phandle, Base, SIZE_, "x4C\x03\x05\x86", "xxxx");
			//FindModelPTR = PointerScanPatternEx(phandle, Base, SIZE_, "\x4C\x03\x0D\x3E", "xxxx");
			//FindModelPTR = PointerScanPatternEx(phandle, Base, SIZE_, "\x00\x00\x00\x00\x00\x00\x00\xEB\x00\x4D\x8B\xCA\x4D\x85\xC9\x74\x00\x4D\x8B\x11\x41\x0F\xB7\x42\x68\x25\x00\x00\x00\x00\xC3","???????x?xxxxxxx?xxxxxxxxx????x");
			FindModelPTR = PointerScanPatternEx(phandle, Base, SIZE_, "\x00\x00\x00\x00\x00\x00\x00\xEB\x00\x4D\x8B\xC1\x4D\x85\xC0\x74\x00\x4D\x8B\x08\x49\x8B\xC1\x48\x83\xC4\x20\x5B\xC3", "???????x?xxxxxxx?xxxxxxxxxxxx");
			//FindModelPTR = ScanPatternEx(phandle, Base, SIZE_, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\xFA\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xD0\x07\x00\xFA", "??????xxxxxxxxxx??xxxxxxxxxxxxxx??????xxxxxxxxxx??????xxxxxx");
			//FindModelPTR = PointerScanPatternEx(phandle, Base, SIZE_, "\x4C\x03\x05\xE6", "xxxx");//0x1DC79A0;
			GlobalPTR = PointerScanPatternEx(phandle, Base, SIZE_, "\x4C\x8D\x05\x00\x00\x00\x00\x4D\x8B\x08\x4D\x85\xC9\x74\x11", "xxx????xxxxxxxx");
			World = PointerScanPatternEx(phandle, Base, SIZE_, "\x48\x8B\x05\x00\x00\x00\x00\x45\x00\x00\x00\x00\x48\x8B\x48\x08\x48\x85\xC9\x74\x07", "xxx????x???xxxxxxxxxx");//0x247F840
			PlayerListAddress = PointerScanPatternEx(phandle, Base, SIZE_, "\x48\x8B\x0D\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x48\x8B\xC8\xE8\x00\x00\x00\x00\x48\x8B\xCF", "xxx????x????xxxx????xxx");//0x1D4D2B0
			ReplayInterfaceAddress = PointerScanPatternEx(phandle, Base, SIZE_, "\x48\x8D\x0D\x00\x00\x00\x00\x48\x8B\xD7\xE8\x00\x00\x00\x00\x48\x8D\x0D\x00\x00\x00\x00\x8A\xD8\xE8\x00\x00\x00\x00\x84\xDB\x75\x13\x48\x8D\x0D", "xxx????xxxx????xxx????xxx????xxxxxxx");//0x1EFD4C8
			BlipAddress = PointerScanPatternEx(phandle, Base, SIZE_, "\x4C\x8D\x05\x00\x00\x00\x00\x0F\xB7\xC1", "xxx????xxx");//0x206B4E0
			//NameAddress = PointerScanPatternEx(phandle, Base, SIZE_, "\x41\x8A\x04\x08", "xxxx");//0x286A700
			NameAddress = PointerScanPatternEx(phandle, Base, SIZE_, "\x48\x8B\x05\x00\x00\x00\x00\xC3\x8A\xD1", "xxx????xxx");//0x286A700
	}
		//SGV();
}

int timer = 0;
int timermax = 72;
void ScriptLoad()
{
	//Update_Model();

	/*std::string test1 = std::to_string(GetHealthMax(GetPlayer()));
    MessageBoxA(NULL, test1.c_str(), "testx", MB_OK);
	std::string test2 = std::to_string(GetHealth(GetPlayer()));
	MessageBoxA(NULL, test2.c_str(), "testx", MB_OK);*/

	if (timer == timermax)
	{
		timer = 0;
	}
	timer++;

	if (FULLGODMODE == 1)
	{
		if (GetGodmode() == 0)
		{
			fullgm(0x1);
			std::cout << "Godmode auto on" << std::endl;
			Sleep(500);
		}
	}

	if (GODMODE == 1)
	{
		if (GetHealth(GetPlayer()) < GetHealthMax(GetPlayer()))
		{
			SetHealth(GetPlayer(), GetHealthMax(GetPlayer()));
		}
	}

	if (VGODMODE == 1)
	{
		if (ReadMemoryOffsets<float>(phandle, Base + World, { 0x8,0xD28,0x280 }) < ReadMemoryOffsets<float>(phandle, Base + World, { 0x8,0xD28,0x2A0}))
		{
			WriteMemoryOffsets<float>(phandle, Base + World, ReadMemoryOffsets<float>(phandle, Base + World, { 0x8,0xD28,0x2A0 }), { 0x8,0xD28,0x280 });
		}
	}


	if (RPLoopONOFF == 1)
	{
		levelchange(5);
		Sleep(rand() % 100 + 10);
		levelchange(0);
	}

	if (GetAsyncKeyState(0x31))
	{

		for (size_t i = 0; i <= 20; i++)
		{
			WriteMemoryOffsets<int>(phandle, Base + World, 1091025877, { 0x8,0xD28,0x30,0x68,0x1A0,0x78 });
			Sleep(50);
		}
	}


	if (PadDropONOFF == 1)
	{

		int countermax = ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, { 0x18,0x110 }) * 10;
		counter = counter + 0x10;
		if (counter > countermax*2)
		{
			counter = 0x0;
		}

		//vec3 = ReadMemoryOffsets<vector3>(phandle, Base + World, { 0x8,0x30, 0x50 });
		//ReadMemoryOffsets<vector3>(phandle, Base + ReplayInterfaceAddress, { 0x18,0x100,counter,0x50 });
		//ReadMemoryOffsets<vector3>(phandle, Base + ReplayInterfaceAddress, { 0x18,0x100,counter,0x30,0x90 });
		//ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, { 0x18,0x100,counter,0x280 });

		if (ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, { 0x18,0x100,counter,0x280 }) != 0 && ReadMemoryOffsets<DWORD64>(phandle, Base + ReplayInterfaceAddress, { 0x18,0x100,counter }) !=  ReadMemoryOffsets<DWORD64>(phandle, Base + World, { 0x8 }))
		{
		vector3 vec3 = GetPos(GetPlayer());
		vec3.z = vec3.z + 10;
		WriteMemoryOffsets<vector3>(phandle, Base + ReplayInterfaceAddress, vec3 , { 0x18,0x100,counter,0x30,0x50 });
		WriteMemoryOffsets<vector3>(phandle, Base + ReplayInterfaceAddress, vec3 , { 0x18,0x100,counter,0x90 });
		WriteMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress,0, { 0x18,0x100,counter,0x280 });

		SetPickup(3732468094, 0x9CA6F755);
		//SetPickup(3732468094, -1332461625);//crash
		}
	}

	if (SpawnMoneyPlayerONOFF == 1)
	{
		if (timer == timermax)
		{
			playerdata playerdatainfo = playerselect(Funktion);
			CREATE_AMBIENT_PICKUP(3732468094, playerdatainfo.vec3.x, playerdatainfo.vec3.y, playerdatainfo.vec3.z, MoneyVal, 0x9CA6F755);
		}
	}

	if (SpawnMoneyONOFF == 1)
	{	
		/*
		vector3 pos;
		pos.x = GetPosX(GetPlayer()) + (float)(((rand() % 5) * 1)) - 5 / 2;
		pos.y = GetPosY(GetPlayer()) + (float)(((rand() % 5) * 1)) - 5 / 2;//+ ((rand() % 100)*1)
		pos.z = GetPosZ(GetPlayer()) + (float)5;
		CREATE_AMBIENT_PICKUP(SpawnMoneypickup, pos.x, pos.y, pos.z, MoneyVal, 0x9CA6F755);
		*/
		int found_players = 0;
		for (size_t i = 0; i < 31; i++)
			{
				playerdata player = playerselect(i);
				float player_distance = sqrt(pow((GetPosX(GetPlayer()) - player.vec3.x), 2) + pow((GetPosY(GetPlayer()) - player.vec3.y), 2));
				if (player_distance < 30)
			{
				found_players = found_players + 1;
				//std::cout << found_players << std::endl ;
				//std::cout << player.name << std::endl ;
			}
		}

		for (size_t i = 0; i < 31; i++)
		{
			playerdata player = playerselect(i);
			float player_distance = sqrt(pow((GetPosX(GetPlayer()) - player.vec3.x), 2) + pow((GetPosY(GetPlayer()) - player.vec3.y), 2));
			//float my_distance = player.vec3.x;
			if (player_distance < 50 && found_players != 0)
			{
				if (timer == timermax / i)
				{
					CREATE_AMBIENT_PICKUP(SpawnMoneypickup, player.vec3.x, player.vec3.y, player.vec3.z, MoneyVal, 0x9CA6F755);
					//Sleep(100 / found_players);
				}
			}
		}
	}

	if (CarCarosONOFF == 1)
	{
		int carmax;
		DWORD64 cars;
		DWORD64 mycar;

		std::vector<unsigned int> caroffset = { 0x10,0x180, };
		std::vector<unsigned int> carmaxoffset = { 0x10,0x190 };
		std::vector<unsigned int> mycaroffset = { 0x8,0xD28, };
		//std::vector<unsigned int> PosOffest = { 0x30,0x50 };
		//std::vector<unsigned int> Pos2Offest = { 0x90 };
		//std::vector<unsigned int> PosMoveOffest = { 0x30, 0x68,0x1A0,0x74 };
		//std::vector<unsigned int> PosMove2Offest = { 0x30,0x68,0x1A0,0x78 };

		carmax = ReadMemoryOffsets<int>(phandle, Base + ReplayInterfaceAddress, carmaxoffset) * 0x10;
		mycar = ReadMemoryOffsets<int>(phandle, Base + World, mycaroffset);

		cars = ReadMemoryOffsets<DWORD64>(phandle, Base + ReplayInterfaceAddress, caroffset);
		cars = ReadMemory<DWORD64>(phandle, cars + carcounter);

		carcounter = carcounter + 0x10;
		if (carcounter > carmax)
		{
			carcounter = 0x0; 
		}

		if (cars != GetVehicle() && FunktionCar == 1)
		{
			WriteMemoryOffsets<int>(phandle, cars + 0x30, value4, { 0x68,0x1A0,0x74 });
		}

		if (cars != GetVehicle() && FunktionCar == 2)
		{
			value = value + 0x4;
			WriteMemoryOffsets<int>(phandle, cars + 0x30, flyg2, { 0x68,0x1A0,0x78 });
		}

		if (cars != GetVehicle() && FunktionCar == 3)
		{
			static vector3 blip;
			blip = get_blip_waypoint();
			blip.z = GetPosZ(GetPlayer()) + 20;
			if (blip.x != 0 && blip.y != 0)
			{
				WriteMemoryOffsets<vector3>(phandle, cars + 0x30, blip, { 0x50 });
				WriteMemory<vector3>(phandle, cars + 0x90, blip);
			}
		}

		if (FunktionCar == 4)
		{
			playerdata playerdatainfo = playerliste(0);

			if (cars != GetVehicle() && playerdatainfo.PlayerAdresse != GetPlayer() && playerdatainfo.vec3.x != 0 && playerdatainfo.name != "")
			{
				WriteMemoryOffsets<vector3>(phandle, cars + 0x30, playerdatainfo.vec3, { 0x50 });
				WriteMemory<vector3>(phandle, cars+ 0x90, playerdatainfo.vec3);
			}
		}

		if (FunktionCar == 5)
		{
			playerdata playerdatainfo = playerselect(Funktion);
			{
				if (cars != GetVehicle() && playerdatainfo.PlayerAdresse != GetPlayer() && playerdatainfo.vec3.x != 0 && playerdatainfo.name != "")
				{
				WriteMemoryOffsets<vector3>(phandle, cars + 0x30, playerdatainfo.vec3, { 0x50 });
				WriteMemory<vector3>(phandle, cars+ 0x90, playerdatainfo.vec3);
				}
			}
		}
	}

	if (RainbowColorsONOFF == 1)
	{
		WriteMemoryOffsets<int>(phandle, Base + World, 255, {0x8,0xD28,0x48,0x20,0xA4});
	}

	if (AntiNPCONOFF == 1)
	{
		while (true)
		{
			if (NPCcounter == 0x0)
			{
				NPCcounter = 0x18;
				break;
			}
			if (NPCcounter == 0x18)
			{
				NPCcounter = 0x30;
				break;
			}
			if (NPCcounter == 0x30)
			{
				NPCcounter = 0x0;
				break;
			}
		}
		if (ReadMemoryOffsets<int>(phandle, Base + World, { 0x8,0x2A8,NPCcounter,0x280 }) != 0 && ReadMemoryOffsets<DWORD64>(phandle, Base + World, { 0x8,0x2A8,NPCcounter }) != GetPlayer())
		{
			WriteMemoryOffsets<int>(phandle, Base + World, 0, { 0x8,0x2A8,NPCcounter,0x280 });
		}
	}
}