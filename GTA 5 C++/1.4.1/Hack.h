#pragma once

#ifndef _HACK_H_
#define _HACK_H_

struct vector3
{
	float x;
	float y;
	float z;
};

void ScriptLoad();
void MemoryLoad();
void fullgmonoff();
void vfullgmonoff();
void gm();
void vgm();
void RP();
void levelchange(int wlevel);
void PedDrop();
void SpawnMoney(int value);
void CARC();
void name();
void ANTINPC();
//void playerliste();
void Teleport(vector3);
void Teleport_to_Waypoint();
void Teleport_to_Objectiv();
void TeleportToPlayer();
void SpawnMoneyPlayer();
void TeleportToPlayerList(int MenuID);

void Create_Crash_Pickup();
void CREATE_VEHICLE(int modelhash, float x, float y, float z, bool tune);
void CREATE_AMBIENT_PICKUP(int pickuphash, float x, float y, float z, int money, int modelhash);

#endif



