#pragma once
//Names change to Subversion names.

//Player
int OFFSET_PLAYER = 0x8;
int OFFSET_ENTITY_POSBASE = 0x30;
int OFFSET_ENTITY_POSBASE_POS = 0x50;
int OFFSET_ENTITY_POS = 0x90; //vector3
int OFFSET_ENTITY_HEALTH = 0x280;
int OFFSET_ENTITY_HEALTH_MAX = 0x284;
int OFFSET_ENTITY_ATTACKER = 0x248;

//player (entity) offsets
int OFFSET_PLAYER_VEHICLE = 0xD10; //ptr to last used vehicle
int OFFSET_PLAYER_INFO = 0x10B8; //playerInfo struct
int OFFSET_PLAYER_INFO_NAME = 0x7C;
int OFFSET_PLAYER_INFO_SWIM_SPD = 0xE4; //swim speed; def 1; float
int	OFFSET_PLAYER_INFO_RUN_SPD = 0xE8; //run speed; def 1; float
int OFFSET_PLAYER_INFO_FRAMEFLAGS = 0x190; //frame flags; DWORD
int OFFSET_PLAYER_INFO_WANTED_CAN_CHANGE = 0x0888; //fWantedCanChange
int OFFSET_PLAYER_INFO_WANTED = 0x088C; //wanted level; DWORD
int OFFSET_PLAYER_INFO_STAMINA = 0xC00; //fStamina, fStaminaMax
int OFFSET_PLAYER_RAGDOLL = 0x10A8; //byte; CPed.noRagdoll: 0x20 = off; 0x00/0x01 = on
int OFFSET_PLAYER_SEATBELT = 0x13EC; //byte; CPed.seatBelt: 0xC8 = off; 0xC9 = on
int OFFSET_PLAYER_INVEHICLE = 0x146B;
int OFFSET_PLAYER_ARMOR = 0x14B0; //armour

								  //vehicle offsets
int OFFSET_VEHICLE_HEALTH = 0x84C; //vehicle health; 0.f-1000.f
int OFFSET_VEHICLE_HANDLING = 0x878;
int OFFSET_VEHICLE_HANDLING_ACCELERATION = 0x4C;
int OFFSET_VEHICLE_HANDLING_BRAKEFORCE = 0x6C;
int OFFSET_VEHICLE_HANDLING_TRACTION_CURVE_MIN = 0x90; //fTractionCurveMin
int OFFSET_VEHICLE_HANDLING_DEFORM_MULTIPLIER = 0xF8; //fDeformationDamageMult
int OFFSET_VEHICLE_HANDLING_UPSHIFT = 0x58;
int OFFSET_VEHICLE_HANDLING_SUSPENSION_FORCE = 0xBC; //fSuspensionForce 
int OFFSET_VEHICLE_BULLETPROOF_TIRES = 0x883; //btBulletproofTires;  (btBulletproofTires & 0x20) ? true : false
int OFFSET_VEHICLE_ALARM_LENGTH = 0x9E4; //dwCarAlarmLength
										 //int OFFSET_VEHICLE_OPENABLE_DOORS = 0xB30; //btOpenableDoors
int OFFSET_VEHICLE_GRAVITY = 0xB7C; //fGravity

									//weapon offsets
int OFFSET_WEAPON_MANAGER = 0x10C8; //from playerbase
int OFFSET_WEAPON_CURRENT = 0x20; //from weapon manager
int OFFSET_WEAPON_AMMOINFO = 0x48; //from weaponbase
int OFFSET_WEAPON_AMMOINFO_MAX = 0x28; //ammoinfo
int OFFSET_WEAPON_AMMOINFO_CUR_1 = 0x08; //ptr lvl 1, ptr 1
int OFFSET_WEAPON_AMMOINFO_CUR_2 = 0x00; //ptr tr lvl 2, ptr 1
int OFFSET_WEAPON_AMMOINFO_CURAMMO = 0x18; //offset to cur ammo
int OFFSET_WEAPON_AMMOINFO_TYPE = 0x0C; //offset to projectile type?
int OFFSET_WEAPON_SPREAD = 0x5C; //float set to 0
int OFFSET_WEAPON_BULLET_DMG = 0x98; //float times 10 (so when 0, it will stay 0)
int OFFSET_WEAPON_RELOAD_MULTIPLIER = 0x114; //float times 10
int OFFSET_WEAPON_RECOIL = 0x2A4; //float set to 0
int OFFSET_WEAPON_MODEL_HASH = 0x14;
int OFFSET_WEAPON_NAME_HASH = 0x10;
int OFFSET_WEAPON_RELOAD_VEHICLE = 0x110;
int OFFSET_WEAPON_RANGE = 0x25C;
int OFFSET_WEAPON_SPINUP = 0x124;
int OFFSET_WEAPON_SPIN = 0x128;
int OFFSET_WEAPON_BULLET_BATCH = 0x100; //dwBulletInBatch
int OFFSET_WEAPON_MUZZLE_VELOCITY = 0xFC; //fMuzzleVelocity
int OFFSET_WEAPON_BATCH_SPREAD = 0x104; //fBatchSpread


int OFFSET_ATTACKER_DISTANCE = 0x18;//changed to 0x18, from 0x10
